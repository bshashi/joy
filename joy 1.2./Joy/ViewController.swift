//
//  ViewController.swift
//  Joy
//
//  Created by Jesus Nieves on 10/3/18.
//  Copyright © 2018 Jesus Nieves. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import NVActivityIndicatorView
import RealmSwift
import SDWebImage
class ViewController: BaseViewController {
    
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var leftConstraint: NSLayoutConstraint!
    var ref:DatabaseReference!
    
    var infoMinX = CGFloat(0)
    var infoWidth = CGFloat(0)
    var firstTime = true
    var firstSee = true
    @IBOutlet var MenuBackButton: UIButton!
    @IBOutlet var menuViewWidth: NSLayoutConstraint!
    @IBOutlet var menuView: UIView!
    var MenuIsVisible = false
    @IBOutlet var menuViewLeading: NSLayoutConstraint!
    @IBOutlet var userImage: UIImageView!
    @IBOutlet var lblUserName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.checkRealmVersion()
        MenuBackButton.isHidden = true
        menuView.isHidden = true
        menuViewWidth.constant = 0
        // Do any additional setup after loading the view, typically from a nib.
        UIApplication.shared.statusBarStyle = .lightContent
        if firstSee {
            firstSee = false
            self.setupInit()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        menuView.isHidden = true
        MenuBackButton.isHidden = true
        menuViewLeading.constant = 0
        menuViewWidth.constant = 0
        MenuIsVisible = false

    }
    
    @IBAction func cmdMenuBackBtn(_ sender: Any) {
        
        if MenuIsVisible {
            menuView.isHidden = true
            MenuBackButton.isHidden = true
            menuViewLeading.constant = 0
            menuViewWidth.constant = 0
            MenuIsVisible = false
        }
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
            self.view.layoutIfNeeded()
        }) { (animationComplete) in
            print("The animation is complete!")
        }
    }
    @IBAction func cmdMenu(_ sender: Any) {
        
        if !MenuIsVisible {
            menuView.isHidden = false
             MenuBackButton.isHidden = false
            menuViewLeading.constant = 0
            menuViewWidth.constant = 280
            MenuIsVisible = true
            
        }
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
            self.view.layoutIfNeeded()
        }) { (animationComplete) in
            print("The animation is complete!")
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !firstSee {
            let realm = try! Realm()
            let purchased = realm.objects(Purchased.self).first
            if let buyed = purchased {
                print("existe el actual asi que no hacemos nada")
            } else {
                self.checkSubscription()
            }
        }
        //DispatchQueue.main.async { [weak self] in
        //    self?.checkSubscription()
        //}
    }
    
    
    override func viewDidLayoutSubviews(){
        if firstTime{
            firstTime = false
            configureInfo()
        }
    }
    
    @IBAction func cmdPrivacy(_ sender: Any) {
        
        guard let url = URL(string: "https://www.findjoyapp.com/privacy") else {
            return
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }   
    
    @IBAction func cmdSafari(_ sender: Any) {
        let url = "https://www.findjoyapp.com"
         UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
        
    }
    
    @IBAction func cmdInsta(_ sender: Any) {
        let url = "https://www.instagram.com/findjoy_app/"
         UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
       
    }
    @IBAction func cmdPin(_ sender: Any) {
        let url = "https://www.pinterest.es/findjoyapp/"
        UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
       
    }
    
    @IBAction func cmdFacebook(_ sender: Any) {
        let url = "https://www.facebook.com/findjoyapp/"
        UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
    }
    func checkSubscription () {
        self.ref = Database.database().reference()
        let deviceId = UIDevice.current.identifierForVendor!.uuidString
        self.ref.child("devices").child(deviceId).observeSingleEvent(of: .value, with: {(snap) in
            if snap.exists(){
                let value = snap.value as? NSDictionary
                if let t = value?["timestamp"] as? TimeInterval {
                    let currentTimeStamp: TimeInterval?
                    let ref = Database.database().reference().child("serverTimestamp")
                    ref.setValue(ServerValue.timestamp())
                    ref.observe(.value, with: { snap in
                        if let tNow = snap.value as? TimeInterval {
                            let firstDate = Date(timeIntervalSince1970: t/1000)
                            let secondDate = Date(timeIntervalSince1970: tNow/1000)
                            let calendar = NSCalendar.current
                            let date1 = calendar.startOfDay(for: firstDate)
                            let date2 = calendar.startOfDay(for: secondDate)
                            let components = calendar.dateComponents([.day], from: date1, to: date2)
                            if (components.day! > 7 ) {
                                let viewController = AppStoryboard.Subscripcion.instance.instantiateViewController(withIdentifier: "SubscripcionVC") as! SubscripcionVC
                                self.present(viewController, animated: false)
                            }
                        }
                    })
                }
            } else {
                let newDevice: [String : Any] = ["device": deviceId, "timestamp": ServerValue.timestamp()]
                self.ref.child("devices").child(deviceId).setValue(newDevice)
            }
        })
    }

    func setupInit() {
        Auth.auth().addStateDidChangeListener({ auth, user in
            if user == nil {
                self.presentLogin()
            }else{
                if let password = UserDefaults.standard.value(forKey: Resources.getEmailUser() + "AskForPassword") as? Bool{
                    if password{
                        self.presentLoginWith(email: (user?.email!)!)
                        return
                    }
                }
                self.ref = Database.database().reference()
                let id:String  = (Auth.auth().currentUser?.uid)!
                self.ref.child("users").child(id).observeSingleEvent(of: .value, with: {(snap) in
                    if snap.exists() {
                        let value = snap.value as? NSDictionary
                        let name:String = (value?["name"] as? String)!
                        let defaults = UserDefaults.standard
                        defaults.set(name, forKey: "name")
                        self.lblUserName.text = name
                        
                        let keyExists = value?["picture"] != nil
                        
                        var stringURL = ""
                        if (keyExists) {
                            stringURL = value?["picture"] as! String
                        }
                        self.userImage.sd_setImage(with: URL(string: stringURL), placeholderImage: UIImage(named: "foto perfil"))
                        defaults.set(stringURL, forKey: "image")
                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                        self.presentWelcomeBack()
                    } else {
                        self.presentLogin()
                    }
                })
            }
        })
    }
    
    func configureInfo(){
        infoLabel.text = NSLocalizedString("Tap on each of the symbols to access the different tools", comment: "")
        infoMinX = self.infoView.frame.origin.x
        infoWidth = self.infoView.frame.width
        let gradient = CAGradientLayer()
        gradient.frame = self.infoView.bounds
        gradient.colors = [#colorLiteral(red: 0.5333333333, green: 0.5058823529, blue: 0.7764705882, alpha: 1).cgColor, #colorLiteral(red: 0.1058823529, green: 0.6980392157, blue: 0.6745098039, alpha: 1).cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        self.infoView.layer.cornerRadius = 16
        self.infoView.layer.insertSublayer(gradient, at: 0)
        DispatchQueue.main.async {
            self.leftConstraint.constant = self.infoView.frame.maxX
        }
    }
    
    @IBAction func info(sender: UIButton){
        sender.isSelected = !sender.isSelected
        UIView.animate(withDuration: 1, delay: 0.0, options: [], animations: {
            self.leftConstraint.constant = sender.isSelected ? self.infoMinX : self.infoView.frame.maxX
            self.view.layoutIfNeeded()
        }, completion:{ (finished: Bool) in
            
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func unwindToHomeViewController(segue: UIStoryboardSegue) {
        print("IM BACK")
    }
    
    func checkRealmVersion (){
        let config = Realm.Configuration(
            schemaVersion: 4,
            migrationBlock: { migration, oldSchemaVersion in
                if oldSchemaVersion < 4 {
                    
                }
        })
        Realm.Configuration.defaultConfiguration = config
    }
    
    func presentLogin (){
        let viewController = AppStoryboard.Login.instance.instantiateViewController(withIdentifier: "LoginNavigation") as! UINavigationController
        self.present(viewController, animated: false)
    }
    func presentLoginWith(email: String){
        let viewController = AppStoryboard.Login.instance.instantiateViewController(withIdentifier: "LoginNavigation") as! UINavigationController
        let loginVC = viewController.childViewControllers[0] as! LoginVC
        loginVC.emailString = email
        self.present(viewController, animated: false)
    }
    func presentWelcomeBack(){
        let viewController = AppStoryboard.Login.instance.instantiateViewController(withIdentifier: "WelcomeBackNavigation") as! UINavigationController
        self.present(viewController, animated: false)
    }
}



