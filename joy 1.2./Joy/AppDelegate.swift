//
//  AppDelegate.swift
//  Joy
//
//  Created by Jesus Nieves on 10/3/18.
//  Copyright © 2018 Jesus Nieves. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
         UIApplication.shared.isStatusBarHidden = true
        UIApplication.shared.statusBarStyle = .lightContent
        UIApplication.shared.isIdleTimerDisabled = true
        
        FirebaseApp.configure()

        return true
    }

}

