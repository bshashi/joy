//
//  CalendarVC.swift
//  calendarIOS
//
//  Created by Macbook pro on 19/5/17.
//  Copyright © 2017 Rhonny Gonzalez. All rights reserved.
//

import UIKit
import JTAppleCalendar
import AFDateHelper

class CalendarVC: UIViewController {
    
    @IBOutlet weak var monthButton: UIButton!
    @IBOutlet weak var calendarView: JTAppleCalendarView!
    @IBOutlet weak var monthLbl: UILabel!
    @IBOutlet weak var yearLbl: UILabel!
    let monthColor = UIColor.black
    let selectedMonthColor = UIColor.white
    let formatter = DateFormatter()
    let headerFormatter = DateFormatter()
    var datePicker = UIDatePicker()
    
    var pickerSelect = false
    
    let todayDate = Date.init()
    var selectedCell: JTAppleCell?
    var selectedCellState: CellState?
    var newArrayOfDates: [[String:Any]] = []
    var arrayOfDates: [String] = []
    var dictOfDates: [[String:String]] = []
    var firstLoad = true
    
    var isSavedDate = false
    var savedColor = UIColor()
    var isColorChanged = false
    let tiredSick = "AFC950"
    let productiveMotivated = "7FB6D9"
    let stressed = "4240AA"
    let happyLoving = "D662A5"
    let sad = "939393"
    let peacefulRelaxed = "2EB2B1"
    let angryGrumpy = "A082DB"
    var colors: [String:Int] = [:]
    var arrayOfMonthsAndColor: [[String:Any]] = []

    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var leftConstraint: NSLayoutConstraint!
    
    var infoMinX = CGFloat(0)
    var infoWidth = CGFloat(0)
    var firstTime = true
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        monthButton.layer.cornerRadius = 17
        monthButton.layer.borderColor = UIColor.white.cgColor
        monthButton.layer.borderWidth = 0.5
        colors = [tiredSick:0,productiveMotivated:0,stressed:0,happyLoving:0,sad:0,peacefulRelaxed:0,angryGrumpy:0]
        arrayOfMonthsAndColor = [["haveColors":true],
                                 ["month":"january","colors":colors],
                                 ["month":"february","colors":colors],
                                 ["month":"march","colors":colors],
                                 ["month":"april","colors":colors],
                                 ["month":"may","colors":colors],
                                 ["month":"june","colors":colors],
                                 ["month":"july","colors":colors],
                                 ["month":"august","colors":colors],
                                 ["month":"september","colors":colors],
                                 ["month":"october","colors":colors],
                                 ["month":"november","colors":colors],
                                 ["month":"december","colors":colors]]
 
        let moodColor = Notification.Name("MoodColor")
        
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(self.mood(notification:)), name: moodColor, object: nil)

        if let dates = UserDefaults.standard.value(forKey: Resources.getEmailUser() + "datesSelected") as? [[String:Any]]{
            newArrayOfDates = dates
        }
        if let arrayWithColors = UserDefaults.standard.value(forKey: Resources.getEmailUser() + "2019 Statics") as? [[String:Any]]{
            arrayOfMonthsAndColor = arrayWithColors
        }
        setupCalendarView()

        calendarView.visibleDates{visibleDates in
            self.setupMonthAndYear(visibleDates: visibleDates)
        }
        
    }
    @IBAction func back(sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func showStatics(sender: UIButton){
       // if arrayOfMonthsAndColor[0]["haveColors"] as! Bool{
            self.performSegue(withIdentifier: "SegueStatistics", sender: self)
        //}
    }
    @IBAction func cmdStats(_ sender: Any) {
        self.performSegue(withIdentifier: "SegueStatistics", sender: self)
    }
    @objc func mood(notification: Notification){
        DispatchQueue.main.async {
            self.isColorChanged = true
            self.handleCellSelected(view: self.selectedCell, cellState: self.selectedCellState!,color: notification.object as! UIColor)
        }
    }
    override func viewDidLayoutSubviews(){
        
        if firstTime{
            firstTime = false
            configureInfo()
        }
        
    }
    func configureInfo(){
        infoLabel.text = NSLocalizedString("How do you feel today? Tap the circles to add your mood every day. This will allow you to have a more objective view of your reality.", comment: "")
        infoMinX = self.infoView.frame.origin.x
        infoWidth = self.infoView.frame.width
        let gradient = CAGradientLayer()
        gradient.frame = self.infoView.bounds
        gradient.colors = [#colorLiteral(red: 0.5333333333, green: 0.5058823529, blue: 0.7764705882, alpha: 1).cgColor, #colorLiteral(red: 0.1058823529, green: 0.6980392157, blue: 0.6745098039, alpha: 1).cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        self.infoView.layer.cornerRadius = 16
        self.infoView.layer.insertSublayer(gradient, at: 0)
        DispatchQueue.main.async {
            self.leftConstraint.constant = self.infoView.frame.maxX
        }
    }
    
    @IBAction func info(sender: UIButton){
        sender.isSelected = !sender.isSelected
        UIView.animate(withDuration: 1, delay: 0.0, options: [], animations: {
            self.leftConstraint.constant = sender.isSelected ? self.infoMinX : self.infoView.frame.maxX
            self.view.layoutIfNeeded()
        }, completion:{ (finished: Bool) in
            
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension CalendarVC: JTAppleCalendarViewDataSource{
   
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        
        formatter.dateFormat = "yyyy MM dd"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
    
        headerFormatter.dateFormat = "MMMM-yyyy"
        headerFormatter.timeZone = Calendar.current.timeZone
        headerFormatter.locale = Calendar.current.locale

        let startDate = formatter.date(from: "2018 01 01")!
        let endDate = formatter.date(from: "2019 12 31")!
        let calendar = Calendar.current
        let parameters = ConfigurationParameters.init(startDate: startDate, endDate: endDate,firstDayOfWeek: .monday)
//        let parameters = ConfigurationParameters(startDate: startDate,
//                                                 endDate: endDate,
//                                                 numberOfRows: 6,
//                                                 calendar: calendar,
//                                                 generateInDates: .forAllMonths,
//                                                 generateOutDates: .tillEndOfGrid,
//                                                 firstDayOfWeek: .monday,
//                                                 hasStrictBoundaries: true)
        
        return parameters
        
    }
    
    
}

extension CalendarVC: JTAppleCalendarViewDelegate{
    
    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        handleCellSelected(view: cell, cellState: cellState,color: .clear)
    }
    
    //Display Cell
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "CustomCell", for: indexPath) as! CustomCell
        
        cell.dateLabel.text = cellState.text
        /*if !cellState.isSelected && cellState.dateBelongsTo == .thisMonth{
        for day in newArrayOfDates{
            if day["date"] as! Date == date{
                isSavedDate = true
                savedColor = UIColor(hex: day["color"] as! String)!
                selectedCell = cell
                calendarView.selectDates([date])
                return cell
            }
        }
        }*/
        handleCellSelected(view: cell, cellState: cellState,color: .clear)
        
        return cell
        
    }
    func compareTodayDate(sender: Date) -> (isToday: Bool,isLessThanToday: Bool){
        let newFormatter = DateFormatter()
        newFormatter.dateFormat = "yyyy-MM-dd"
        newFormatter.timeZone = Calendar.current.timeZone
        newFormatter.locale = Calendar.current.locale
        let str = String(describing: sender).components(separatedBy: " ")[0]
        let today = String(describing: todayDate).components(separatedBy: " ")[0]
        if str == today{
            return (true,false)
        }else if newFormatter.date(from: str)! < newFormatter.date(from: today)!{
            return (false,true)
        }
            return (false,false)
    }
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        
        if isSavedDate{
//            selectedCellState = cellState
            self.handleCellSelected(view: cell, cellState: cellState,color: savedColor)
            return
        }
        guard let validCell = cell as? CustomCell else {return}
        selectedCell = cell
        selectedCellState = cellState
//        if isSavedDate{
//            isSavedDate = false
//            self.handleCellSelected(view: self.selectedCell, cellState: self.selectedCellState!,color: savedColor)
//            return
//        }
        let dateToShow = date.toString(.custom("EEEE-dd-MMMM"))
        let dict = ["date":dateToShow,"color":validCell.normalView.backgroundColor!] as NSDictionary
        self.performSegue(withIdentifier: "SegueDay", sender: dict)

    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SegueDay"{
            let dvc = segue.destination as! DayVC
            dvc.dateSelected = (sender as! NSDictionary)["date"] as! String
            dvc.color = (sender as! NSDictionary)["color"] as! UIColor
        }else{
            let svc = segue.destination as! StaticsVC
            svc.arrayOfMonthsAndColor = arrayOfMonthsAndColor
        }
    }
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        setupMonthAndYear(visibleDates: visibleDates)
    }
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        
        guard let validCell = cell as? CustomCell else {return}
        calendarView.selectDates([date])
        return

    }
}


extension CalendarVC {
    
    func saveColor(color: String, month: Int, year: String){
        arrayOfMonthsAndColor[0]["haveColors"]! = true
        let key = Resources.getEmailUser() + year + " Statics"
        var dictColor = arrayOfMonthsAndColor[month]["colors"] as! [String:Int]
        dictColor[color]! += 1
        arrayOfMonthsAndColor[month]["colors"]! = dictColor
        UserDefaults.standard.setValue(arrayOfMonthsAndColor, forKey: key)
    }
    func setupCalendarView(){
        calendarView.isHidden = true
        calendarView.minimumLineSpacing = 0
        calendarView.minimumInteritemSpacing = 2.5
        calendarView.allowsMultipleSelection = true

        calendarView.scrollToDate(todayDate){
            for i in 0 ..< self.newArrayOfDates.count{
                self.isSavedDate = true
                let color = self.newArrayOfDates[i]["color"] as! String
                self.savedColor = color != "" ? UIColor(hex: color)! : UIColor.clear
                self.calendarView.selectDates([self.newArrayOfDates[i]["date"] as! Date])
            }
            self.isSavedDate = false
            self.calendarView.isHidden = false
        }
        
    }
    @IBAction func prevMonth(sender: UIButton){
        sender.isUserInteractionEnabled = false
        //        calendarView.scrollToSegment(.previous, completionHandler: { void in
        //            sender.isUserInteractionEnabled = true
        //        })
        calendarView.scrollToSegment(.previous){
            sender.isUserInteractionEnabled = true
        }
    }
    @IBAction func nextMonth(sender: UIButton){
        sender.isUserInteractionEnabled = false
        //        calendarView.scrollToSegment(.next, completionHandler: { void in
        //            sender.isUserInteractionEnabled = true
        //        })
        calendarView.scrollToSegment(.next){
            sender.isUserInteractionEnabled = true
        }
    }
    func setupMonthAndYear(visibleDates: DateSegmentInfo){
        let date = visibleDates.monthDates.first!.date
        let separatedDate = headerFormatter.string(from: date).components(separatedBy: "-")
        
        monthLbl.text = separatedDate[0].uppercased()
        yearLbl.text = separatedDate[1]
        
    }
    func handleCellSelected(view: JTAppleCell?, cellState: CellState,color: UIColor){
        
        guard let validCell = view as? CustomCell else {return}
        
        validCell.normalView.frame = CGRect(x: (validCell.frame.size.width * 0.05), y: (validCell.dateLabel.center.y) - (validCell.frame.size.width * 0.45), width: validCell.frame.size.width * 0.9, height: validCell.frame.size.width * 0.9)
        validCell.normalView.layer.borderColor = UIColor.white.cgColor
        validCell.normalView.layer.borderWidth = 1
        validCell.normalView.layer.cornerRadius = validCell.normalView.frame.width / 2
        
        if  (cellState.isSelected && cellState.dateBelongsTo == .thisMonth) {
            var newColor = color
            if let index = newArrayOfDates.index(where: { ($0["date"] as! Date) == cellState.date }){
                if isColorChanged && color != .clear{
                    isColorChanged = false
                    newArrayOfDates[index] = ["date":cellState.date,"color":color.toHex ?? ""]
                    UserDefaults.standard.setValue(newArrayOfDates, forKey: Resources.getEmailUser() + "datesSelected")
                    saveColor(color: color.toHex ?? "", month: cellState.date.month(), year: String(describing: cellState.date.year()))
                }else{
                    newColor = UIColor(hex: newArrayOfDates[index]["color"] as! String)!
                }
                //newArrayOfDates.contains(where: { ($0["date"] as! Date) == cellState.date }){
            }else{
                if color != .clear{
                    newArrayOfDates.append(["date":cellState.date,"color":color.toHex ?? ""])
                    UserDefaults.standard.setValue(newArrayOfDates, forKey: Resources.getEmailUser() + "datesSelected")
                    saveColor(color: color.toHex ?? "", month: cellState.date.month(), year: String(describing: cellState.date.year()))
                }
            }
            if newColor != .clear{
                validCell.normalView.layer.borderWidth = 0
                validCell.normalView.layer.cornerRadius = validCell.normalView.frame.width / 2
            }
            validCell.isHidden = false
            validCell.dateLabel.textColor = .white
            validCell.normalView.backgroundColor = newColor
            
        }else if cellState.dateBelongsTo != .thisMonth{
            validCell.isHidden = true
            
        }else{
            validCell.isHidden = false
            validCell.dateLabel.textColor = .white
            validCell.normalView.backgroundColor = UIColor.clear
            validCell.normalView.layer.borderColor = UIColor.white.cgColor
            validCell.normalView.layer.borderWidth = 1
            validCell.normalView.layer.cornerRadius = validCell.normalView.frame.width / 2
        }
        
    }
    
    func handleCellDeSelected(view: JTAppleCell?, cellState: CellState){
        
        guard let validCell = view as? CustomCell else {return}
        
        validCell.selectedView.isHidden = true
        validCell.dateLabel.textColor = .white
        
        print (cellState.date)
        calendarView.deselectDates(from: cellState.date, to: cellState.date, triggerSelectionDelegate: false)
        self.calendarView.reloadData()
        
    }
    
    func correctHour(hour: Int, minute: Int, a: String) -> String{
        var newHour = ""
        if hour < 10 {
            newHour = "0\(hour)"
        }else{
            newHour = "\(hour)"
        }
        if minute >= 0 && minute < 15{
            if a == "AM" || a == "PM"{
                return "\(newHour):00 \(a)"
            }else{
                return "\(newHour):00:\(a)"
            }
        }else if minute >= 15 && minute < 30{
            if a == "AM" || a == "PM"{
                return "\(newHour):15 \(a)"
            }else{
                return "\(newHour):15:\(a)"
            }
        }else if minute >= 30 && minute < 45{
            if a == "AM" || a == "PM"{
                return "\(newHour):30 \(a)"
            }else{
                return "\(newHour):30:\(a)"
            }
        }else{
            if a == "AM" || a == "PM"{
                return "\(newHour):45 \(a)"
            }else{
                return "\(newHour):45:\(a)"
            }
        }
    }
    
}

extension UIColor {
    
    // MARK: - Initialization
    
    convenience init?(hex: String) {
        var hexSanitized = hex.trimmingCharacters(in: .whitespacesAndNewlines)
        hexSanitized = hexSanitized.replacingOccurrences(of: "#", with: "")
        
        var rgb: UInt32 = 0
        
        var r: CGFloat = 0.0
        var g: CGFloat = 0.0
        var b: CGFloat = 0.0
        var a: CGFloat = 1.0
        
        let length = hexSanitized.characters.count
        
        guard Scanner(string: hexSanitized).scanHexInt32(&rgb) else { return nil }
        
        if length == 6 {
            r = CGFloat((rgb & 0xFF0000) >> 16) / 255.0
            g = CGFloat((rgb & 0x00FF00) >> 8) / 255.0
            b = CGFloat(rgb & 0x0000FF) / 255.0
            
        } else if length == 8 {
            r = CGFloat((rgb & 0xFF000000) >> 24) / 255.0
            g = CGFloat((rgb & 0x00FF0000) >> 16) / 255.0
            b = CGFloat((rgb & 0x0000FF00) >> 8) / 255.0
            a = CGFloat(rgb & 0x000000FF) / 255.0
            
        } else {
            return nil
        }
        
        self.init(red: r, green: g, blue: b, alpha: a)
    }
    
    // MARK: - Computed Properties
    
    var toHex: String? {
        return toHex()
    }
    
    // MARK: - From UIColor to String
    
    func toHex(alpha: Bool = false) -> String? {
        guard let components = cgColor.components, components.count >= 3 else {
            return nil
        }
        
        let r = Float(components[0])
        let g = Float(components[1])
        let b = Float(components[2])
        var a = Float(1.0)
        
        if components.count >= 4 {
            a = Float(components[3])
        }
        
        if alpha {
            return String(format: "%02lX%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255), lroundf(a * 255))
        } else {
            return String(format: "%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255))
        }
    }
    
}

