//
//  ObjectivesTVCell.swift
//  Joy
//
//  Created by Rhonny Gonzalez on 4/6/18.
//  Copyright © 2018 Jesus Nieves. All rights reserved.
//

import UIKit

class ObjectivesTVCell: UITableViewCell {

    @IBOutlet weak var textField: CustomSearchTextField!
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var deleteWidth: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
