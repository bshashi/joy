//
//  NotebookChildVC.swift
//  Joy
//
//  Created by Rhonny Gonzalez on 29/5/18.
//  Copyright © 2018 Jesus Nieves. All rights reserved.
//

import UIKit
import RealmSwift
import FirebaseAuth

class NotebookChildVC: UIViewController {

    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var textView: UITextView!
    var maxY: CGFloat = 0.0
    var keyboardHeight: CGFloat = 0.0
    var notebookDateKey = ""
    var maxNumberOfLines = 11
    private var currentNotebook:Notebook!
    private var currentPage:NotebookPage!
    private let realm = try! Realm()
    private var currentIndex:Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.currentIndex = UserDefaults.standard.value(forKey: "CurrentIndex") as? Int ?? 0
        textView.font = UIFont(name: "Ecosmith Script", size: 17.0)
        textView.delegate = self
        textView.textContainer.maximumNumberOfLines = maxNumberOfLines
        textView.text = ""
        
        if let key = UserDefaults.standard.value(forKey: Resources.getEmailUser() + "notebookDate") as? String{
            notebookDateKey = key
        }else{
            let formatter = DateFormatter()
            formatter.timeZone = TimeZone(secondsFromGMT: 0)
            formatter.dateFormat = "dd/MM/yyyy"
            let todayDate = formatter.date(from: "\(Date().preciseLocalDate)")
            notebookDateKey = todayDate!.preciseGMTDate
        }
        
        let predicate = NSPredicate(format: "key == %@ && user == %@", notebookDateKey, (Auth.auth().currentUser?.uid)!)
        
       self.currentNotebook = self.realm.objects(Notebook.self).filter(predicate).first
        
        textView.text = self.currentNotebook.pages[self.currentIndex].content
        

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name:Notification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name:Notification.Name.UIKeyboardWillHide, object: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //if self.view.frame.size.height < 480 {
            topConstraint.constant = -60
        
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
extension NotebookChildVC: UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        let line = textView.caretRect(for: (textView.selectedTextRange?.start)!)
        let frame = view.convert(line, from:self.view)
        maxY = frame.maxY
    }
    func textViewDidChange(_ textView: UITextView) {
        let line = textView.caretRect(for: (textView.selectedTextRange?.start)!)
        let frame = view.convert(line, from:self.view)
        maxY = frame.maxY
        if maxY > self.view.frame.size.height - keyboardHeight{
            self.view.frame.origin.y = self.view.frame.size.height - keyboardHeight - maxY - 20
        }else{
            self.view.frame.origin.y = 0
        }
        let value = textView.removeTextUntilSatisfying(maxNumberOfLines: maxNumberOfLines)
        if value{
            print ("true")
        }

        //New
        try! self.realm.write {
            self.currentNotebook.pages[self.currentIndex].content = textView.text
        }

    }
    
    @objc func keyboardWillShow(notification:Notification){
        //let frame = firstView.convert(buttons.frame, from:secondView)
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardSize2 = keyboardSize.height
            keyboardHeight = keyboardSize2
            if maxY > self.view.frame.size.height - keyboardSize2{
                self.view.frame.origin.y = self.view.frame.size.height - keyboardSize2 - maxY - 20
            }
            
        }
        
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n" && textView.trueNumberOfLines() == maxNumberOfLines) {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    @objc func keyboardWillHide(notification:Notification){
        
        /*  let contentInset:UIEdgeInsets = UIEdgeInsets.zero
         self.scrollV.contentInset = contentInset*/
        self.view.frame.origin.y = 0
    }
    
    @objc func dismissScreen(){
        
        self.view.endEditing(true)
    }

}
extension UITextView{
    
    func trueNumberOfLines() -> Int{
        let layoutManager:NSLayoutManager = self.layoutManager
        let numberOfGlyphs = layoutManager.numberOfGlyphs
        var numberOfLines = 0
        var index = 0
        var lineRange:NSRange = NSRange()
        
        while (index < numberOfGlyphs) {
            layoutManager.lineFragmentRect(forGlyphAt: index, effectiveRange: &lineRange)
            index = NSMaxRange(lineRange);
            numberOfLines = numberOfLines + 1
        }
        
        return numberOfLines
    }
    var numberOfCurrentlyDisplayedLines: Int {
        let size = systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        return Int(((size.height - layoutMargins.top - layoutMargins.bottom) / font!.lineHeight))
    }
    
    /// Removes last characters until the given max. number of lines is reached
    func removeTextUntilSatisfying(maxNumberOfLines: Int) -> Bool {
        var bool = false
        while trueNumberOfLines() > (maxNumberOfLines) {
            text = String(text.dropLast())
            layoutIfNeeded()
            bool = true
        }
        return bool
    }
    
    
}
