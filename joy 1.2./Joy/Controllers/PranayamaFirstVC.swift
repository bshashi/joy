//
//  PranayamaFirstVC.swift
//  Joy
//
//  Created by Rhonny Gonzalez on 20/6/18.
//  Copyright © 2018 Jesus Nieves. All rights reserved.
//

import UIKit

class PranayamaFirstVC: UIViewController {

    @IBOutlet weak var pickerTime: UIPickerView!
    var minutes = ["1:00","3:00","5:00","7:00","10:00","15:00","20:00"]
    var oldView: UIView?
    var selectedRow = 2
    
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var leftConstraint: NSLayoutConstraint!
    
    var infoMinX = CGFloat(0)
    var infoWidth = CGFloat(0)
    var firstTime = true

    override func viewDidLoad() {
        super.viewDidLoad()
        pickerTime.delegate = self
        pickerTime.dataSource = self
        pickerTime.selectRow(selectedRow, inComponent: 0, animated: false)
        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if firstTime{
            firstTime = false
            configureInfo()
        }
    }
    func configureInfo(){
        infoLabel.text = NSLocalizedString("Sit with a long spine, in a comfortable position, and focus on your breath. If you get distracted, just come back.", comment: "")
        infoMinX = self.infoView.frame.origin.x
        infoWidth = self.infoView.frame.width
        let gradient = CAGradientLayer()
        gradient.frame = self.infoView.bounds
        gradient.colors = [#colorLiteral(red: 0.5333333333, green: 0.5058823529, blue: 0.7764705882, alpha: 1).cgColor, #colorLiteral(red: 0.1058823529, green: 0.6980392157, blue: 0.6745098039, alpha: 1).cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        self.infoView.layer.cornerRadius = 16
        self.infoView.layer.insertSublayer(gradient, at: 0)
        DispatchQueue.main.async {
            self.leftConstraint.constant = self.infoView.frame.maxX
        }
    }
    
    @IBAction func info(sender: UIButton){
        sender.isSelected = !sender.isSelected
        UIView.animate(withDuration: 1, delay: 0.0, options: [], animations: {
            self.leftConstraint.constant = sender.isSelected ? self.infoMinX : self.infoView.frame.maxX
            self.view.layoutIfNeeded()
        }, completion:{ (finished: Bool) in
            
        })
    }
    @IBAction func back(sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "SegueVideo"{
            let tvvc = segue.destination as! TryVideoVC
            tvvc.timeToLast = Int(minutes[selectedRow].components(separatedBy: ":")[0])!
        }
    }

}

extension PranayamaFirstVC: UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return minutes.count
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {

        let pickerLabel = UILabel()
        pickerLabel.tag = row
        pickerLabel.textColor = UIColor.white
        pickerLabel.text = minutes[row]
        pickerLabel.font = UIFont.systemFont(ofSize: selectedRow == row ? 40 : 20)
        pickerLabel.textAlignment = .center
        return pickerLabel
        
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedRow = row
        pickerView.reloadAllComponents()
    }
    
}
