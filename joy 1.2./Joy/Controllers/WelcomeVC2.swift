//
//  WelcomeVC2.swift
//  Joy
//
//  Created by Gourav on 25/06/19.
//  Copyright © 2019 Jesus Nieves. All rights reserved.
//

import UIKit

class WelcomeVC2: UIViewController {
    @IBOutlet weak var welcomeLbl: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        welcomeLbl.text = String(format: "But first, I want you to take the deepest breath of your day:".localized)

    }
    @IBAction func next(sender: UIButton){
        
        self.performSegue(withIdentifier: "welcome3", sender: self)
    }

}
