//
//  BaseViewController.swift
//  Joy
//
//  Created by Jesus Nieves on 10/3/18.
//  Copyright © 2018 Jesus Nieves. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class BaseViewController: UIViewController {
    
    let activityData = ActivityData()
    
    public enum AppStoryboard : String {
        case Main, Login, Subscripcion
        var instance : UIStoryboard {
            return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
