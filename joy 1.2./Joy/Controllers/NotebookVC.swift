    //
//  NotebookVC.swift
//  Joy
//
//  Created by Jesus Nieves              on 29/5/18.
//  Copyright © 2018 Jesus Nieves. All rights reserved.
//

import UIKit
import FirebaseAuth
import RealmSwift

class NotebookVC: UIViewController {

    @IBOutlet weak var datesTF: UITextField!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    
    
    var picker = UIPickerView()
    
    public var appInstallDate: Date {
        if let documentsFolder = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last {
            if let installDate = try! FileManager.default.attributesOfItem(atPath: documentsFolder.path)[.creationDate] as? Date {
                return installDate
            }
        }
        return Date()
    }
    var dates: [String] = []
    var notebooks = [Notebook]()
    
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var leftConstraint: NSLayoutConstraint!
    
    var infoMinX = CGFloat(0)
    var infoWidth = CGFloat(0)
    var firstTime = true
    
    var selectedRow = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let installComponents = Calendar.current.dateComponents([.day,.month,.year], from: appInstallDate)
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "dd/MM/yyyy"
        let installDate = formatter.date(from: "\(installComponents.day!)/\(installComponents.month!)/\(installComponents.year!)")
        let todayDate = formatter.date(from: "\(Date().preciseLocalDate)")
        dates = Date().generateDatesArray(with: installDate!, endDate: todayDate!)

        let realm = try! Realm()
        let predicate = NSPredicate(format: "key == %@ && user == %@", dates.last!, (Auth.auth().currentUser?.uid)!)
        let currentNotebook = realm.objects(Notebook.self).filter(predicate).first
        
        if let book = currentNotebook {
            print("existe el actual")
        } else {
            let newbook = Notebook()
            newbook.key = dates.last!
            newbook.user = (Auth.auth().currentUser?.uid)!
            try! realm.write {
                realm.add(newbook)
            }
        }
        
        let predicateNotebooks = NSPredicate(format: "user == %@", (Auth.auth().currentUser?.uid)!)
        self.notebooks = realm.objects(Notebook.self).filter(predicateNotebooks).toArray(ofType: Notebook.self) as [Notebook]

        UserDefaults.standard.setValue(Resources.getEmailUser() + "Notebook\(installComponents.year!)", forKey: Resources.getEmailUser() + "NotebookKey")
        UserDefaults.standard.setValue(self.notebooks.last?.key, forKey: Resources.getEmailUser() + "notebookDate")
        UserDefaults.standard.setValue(self.notebooks.last?.key, forKey: Resources.getEmailUser() + "LastDate")
        
        selectedRow = self.notebooks.count - 1
        leftButton.imageView?.contentMode = .scaleAspectFit
        rightButton.imageView?.contentMode = .scaleAspectFit
        picker.delegate = self
        picker.dataSource = self
        datesTF.inputView = picker
        picker.selectRow(self.notebooks.count - 1, inComponent: 0, animated: false)
        datesTF.text = self.notebooks.last?.key
        showDay()
    }

    func showDay(){
        let notificationName = Notification.Name("LoadViewControllers")
        
        NotificationCenter.default.post(name: notificationName, object: nil)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        datesTF.layer.borderColor = UIColor.white.cgColor
        datesTF.layer.borderWidth = 1
        datesTF.layer.cornerRadius = 15
        if firstTime{
            firstTime = false
            configureInfo()
        }
    }

    func configureInfo(){
        infoLabel.text = NSLocalizedString("Inhale, exhale and write the first thing that crosses your mind.", comment: "")
        infoMinX = self.infoView.frame.origin.x
        infoWidth = self.infoView.frame.width
        let gradient = CAGradientLayer()
        gradient.frame = self.infoView.bounds
        gradient.colors = [#colorLiteral(red: 0.5333333333, green: 0.5058823529, blue: 0.7764705882, alpha: 1).cgColor, #colorLiteral(red: 0.1058823529, green: 0.6980392157, blue: 0.6745098039, alpha: 1).cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        self.infoView.layer.cornerRadius = 16
        self.infoView.layer.insertSublayer(gradient, at: 0)
        DispatchQueue.main.async {
            self.leftConstraint.constant = self.infoView.frame.maxX
        }
    }

    @IBAction func leftButtonAction(sender: UIButton){
        if selectedRow > 0 {
            selectedRow -= 1
            picker.selectRow(selectedRow, inComponent: 0, animated: false)
            datesTF.text = self.notebooks[selectedRow].key
            UserDefaults.standard.setValue(self.notebooks[selectedRow].key, forKey: Resources.getEmailUser() + "notebookDate")
            
            let notificationName = Notification.Name("LoadViewControllers")
            NotificationCenter.default.post(name: notificationName, object: "reverse")
            self.datesTF.resignFirstResponder()
        }
    }

    @IBAction func rightButtonAction(sender: UIButton){
        if selectedRow < self.notebooks.count - 1 {
            selectedRow += 1
            picker.selectRow(selectedRow, inComponent: 0, animated: false)

            datesTF.text = self.notebooks[selectedRow].key
            UserDefaults.standard.setValue(self.notebooks[selectedRow].key, forKey: Resources.getEmailUser() + "notebookDate")
            let notificationName = Notification.Name("LoadViewControllers")
            
            NotificationCenter.default.post(name: notificationName, object: nil)
            self.datesTF.resignFirstResponder()
        }
    }

    @IBAction func info(sender: UIButton){
        sender.isSelected = !sender.isSelected
        UIView.animate(withDuration: 1, delay: 0.0, options: [], animations: {
            self.leftConstraint.constant = sender.isSelected ? self.infoMinX : self.infoView.frame.maxX
            self.view.layoutIfNeeded()
        }, completion:{ (finished: Bool) in
            
        })
    }

    @IBAction func done(sender: Any){
        let notificationName = Notification.Name("NextPage")
        
        NotificationCenter.default.post(name: notificationName, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func back(sender: UIButton){
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }

}
extension NotebookVC: UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.notebooks.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.notebooks[row].key
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if selectedRow != row {
        
        datesTF.text = self.notebooks[row].key
        UserDefaults.standard.setValue(self.notebooks[row].key, forKey: Resources.getEmailUser() + "notebookDate")

        let notificationName = Notification.Name("LoadViewControllers")
        if row < selectedRow {
           NotificationCenter.default.post(name: notificationName, object: "reverse")
        }
        NotificationCenter.default.post(name: notificationName, object: nil)
        selectedRow = row
        }
        self.datesTF.resignFirstResponder()
    }
}

