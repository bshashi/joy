//
//  ProposalsVC.swift
//  Joy
//
//  Created by Rhonny Gonzalez on 15/5/18.
//  Copyright © 2018 Jesus Nieves. All rights reserved.
//

import UIKit

class ProposalsVC: ExpandingViewController {

    private var isFlipped: Bool = false
//    typealias ItemInfo = (imageName: String, title: String)
    fileprivate var cellsIsOpen = [Bool]()
    var items: [ItemInfo] = []
    @IBOutlet weak var proposalIV: UIImageView!
    @IBOutlet weak var proposalLbl: UILabel!
    @IBOutlet var buttons: [UIButton]!
    var proposals: NSArray = []
    var typeLanguage = ""
    
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var leftConstraint: NSLayoutConstraint!
    
    var infoMinX = CGFloat(0)
    var infoWidth = CGFloat(0)
    var firstTime = true

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        //proposalIV.setRounded()
        
        if firstTime{
            firstTime = false
            configureInfo()
        }
        
    }
    func configureInfo(){
        infoLabel.text = NSLocalizedString("I show you three daily activities that will make you feel great and reinforce your self-esteem.", comment: "")
        infoMinX = self.infoView.frame.origin.x
        infoWidth = self.infoView.frame.width
        let gradient = CAGradientLayer()
        gradient.frame = self.infoView.bounds
        gradient.colors = [#colorLiteral(red: 0.5333333333, green: 0.5058823529, blue: 0.7764705882, alpha: 1).cgColor, #colorLiteral(red: 0.1058823529, green: 0.6980392157, blue: 0.6745098039, alpha: 1).cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        self.infoView.layer.cornerRadius = 16
        self.infoView.layer.insertSublayer(gradient, at: 0)
        DispatchQueue.main.async {
            self.leftConstraint.constant = self.infoView.frame.maxX
        }
    }
    
    @IBAction func info(sender: UIButton){
        sender.isSelected = !sender.isSelected
        UIView.animate(withDuration: 1, delay: 0.0, options: [], animations: {
            self.leftConstraint.constant = sender.isSelected ? self.infoMinX : self.infoView.frame.maxX
            self.view.layoutIfNeeded()
        }, completion:{ (finished: Bool) in
            
        })
    }
    
    ///Returns data from Proposals.json and fill up proposals array with three random items.
    func getProposals(){
        /*if let path = Bundle.main.path(forResource: "Proposals", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? NSArray{*/
                    let jsonResult = Resources.getProposals()
                    proposals = jsonResult.threeRandomItems()
                    let key = Resources.getEmailUser() + "Proposal \(Date().preciseLocalDate)"
                    UserDefaults.standard.setValue(proposals, forKey: key)
                    if let lastProposalDate = UserDefaults.standard.value(forKey: Resources.getEmailUser() + "LastProposals") as? String{
                        UserDefaults.standard.removeObject(forKey: lastProposalDate)
                    }
                    UserDefaults.standard.setValue(key, forKey: Resources.getEmailUser() + "LastProposals")
                /*}
            } catch {
                #if DEBUG
                print("Error getting proposals")
                #endif
            }
        }*/
    }
    
    @IBAction func cmdChangeCardBack(_ sender: Any) {
    }
    ///Configure round image and buttons attributes.
    func configure(){
        
        
        typeLanguage = checkLanguage()
        if let savedProposals = UserDefaults.standard.value(forKey: Resources.getEmailUser() + "Proposal \(Date().preciseLocalDate)") as? NSArray{
            proposals = savedProposals
        }else{
            getProposals()
        }
        
        for dict in proposals{
            items.append(ItemInfo(values: dict as! Dictionary<String, Any>))
        }
        
//        proposalLbl.text = ((proposals[0] as! NSDictionary)[typeLanguage] as! String)
//        proposalIV.image = UIImage(named: ((proposals[0] as! NSDictionary)["imageName"] as! String))
//        proposalLbl.font = UIFont(name: "Caviar Dreams", size: 20.0)
//        for i in 0 ..< buttons.count{
//            buttons[i].layer.cornerRadius = 24
//            buttons[i].layer.borderColor = UIColor.white.cgColor
//            buttons[i].layer.borderWidth = 1
//            buttons[i].tag = i
//            buttons[i].addTarget(self, action: #selector(selected(sender:)), for: .touchUpInside)
//        }
//        buttons[0].layer.borderWidth = 0
//        buttons[0].backgroundColor = #colorLiteral(red: 0.2534175217, green: 0.2556501329, blue: 0.6728743315, alpha: 1)
        
    }
    
    ///Allow buttons change background and also changes personalLbl text.
    @objc func selected(sender:UIButton){
        for i in 0 ..< buttons.count{
            if i == sender.tag{
                buttons[i].layer.borderWidth = 0
                buttons[i].backgroundColor = #colorLiteral(red: 0.2534175217, green: 0.2556501329, blue: 0.6728743315, alpha: 1)
                proposalLbl.text = ((proposals[i] as! NSDictionary)[typeLanguage] as! String)
                proposalLbl.font = UIFont(name: "Caviar Dreams", size: 20.0)
                proposalIV.image = UIImage(named: ((proposals[i] as! NSDictionary)["imageName"] as! String))
            }else{
                buttons[i].layer.borderWidth = 1
                buttons[i].backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.01)
            }
        }
    }

    @IBAction func back(sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func checkLanguage() -> String{
        var lenguajesUsuario:[String] = Locale.preferredLanguages
        let misLenguajes:Set<String> = ["es-","en-"]
        
        
        let index = lenguajesUsuario.index { (lenguaje) -> Bool in
            return misLenguajes.contains(String(lenguaje.prefix(3)))
        }
        
        guard let safeIndex = index else{

            ///ingles
            return "text-en"
        }
        
        let lenguaje = String(lenguajesUsuario[safeIndex].prefix(2))
        return "text-\(lenguaje)"
        
        
    }

}


extension ProposalsVC {
    
    override func viewDidLoad() {
        itemSize = CGSize(width: 200, height: 300)
        super.viewDidLoad()
        
        configure()
        registerCell()
        fillCellIsOpenArray()
    }
}
// MARK: Helpers

extension ProposalsVC {
    
    fileprivate func registerCell() {
        
        let nib = UINib(nibName: String(describing: DemoCollectionViewCell.self), bundle: nil)
        collectionView?.register(nib, forCellWithReuseIdentifier: String(describing: DemoCollectionViewCell.self))
    }
    
    fileprivate func fillCellIsOpenArray() {
        cellsIsOpen = Array(repeating: false, count: items.count)
    }
}

// MARK: UIScrollViewDelegate

extension ProposalsVC {
    
    func scrollViewDidScroll(_: UIScrollView) {
//        pageLabel.text = "\(currentIndex + 1)/\(items.count)"
    }
}

// MARK: UICollectionViewDataSource

extension ProposalsVC {
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        super.collectionView(collectionView, willDisplay: cell, forItemAt: indexPath)
        guard let cell = cell as? DemoCollectionViewCell else { return }
        
        let index = indexPath.row % items.count
        let info = items[index]
        cell.backgroundImageView?.image = UIImage(named: "card")
        //        cell.customTitle.text = info.title
        cell.foregroundImageView.image = UIImage(named: "card")
        cell.cellIsOpen(cellsIsOpen[index], animated: false)
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        isFlipped = !isFlipped
        guard let cell = collectionView.cellForItem(at: indexPath) as? DemoCollectionViewCell else { return }
        let cardToFlip = isFlipped ? cell.goldenView : cell.greenView
        let bottomCard = isFlipped ? cell.greenView : cell.goldenView
        UIView.transition(from: cardToFlip!,
                          to: bottomCard!,
                          duration: 0.5,
                          options: [.transitionFlipFromRight, .showHideTransitionViews],
                          completion: nil)
        
    }
}

// MARK: UICollectionViewDataSource

extension ProposalsVC {
    
    override func collectionView(_: UICollectionView, numberOfItemsInSection _: Int) -> Int {
        return items.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: DemoCollectionViewCell.self), for: indexPath)
    }
}

struct ItemInfo {
    var imageName: String
    var textEn:String
    var textes:String
    
    init(values:Dictionary<String,Any>) {
        
        imageName = values["imageName"] as! String
        textEn = values["text-en"] as! String
        textes = values["text-es"] as! String
    }
}
