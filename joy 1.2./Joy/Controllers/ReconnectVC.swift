//
//  ReconnectVC.swift
//  Joy
//
//  Created by Rhonny Gonzalez on 7/6/18.
//  Copyright © 2018 Jesus Nieves. All rights reserved.
//

import UIKit
import UserNotifications

class ReconnectVC: UIViewController {
    
    @IBOutlet weak var enableButton: UIButton!
    @IBOutlet weak var scheduleSlider: RangeSlider!
    @IBOutlet weak var repetitionsSlider: RangeSlider!
    @IBOutlet weak var soundButton: UIButton!
    
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var leftConstraint: NSLayoutConstraint!
    
    var infoMinX = CGFloat(0)
    var infoWidth = CGFloat(0)
    var firstTime = true
    override func viewDidLoad() {
        super.viewDidLoad()
        

        let notificationName = Notification.Name("disableNotification")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.disableNotification(notification:)), name: notificationName, object: nil)
        var active = true
        if let sound = UserDefaults.standard.value(forKey: Resources.getEmailUser() + "ReconectionSound") as? Bool{
            active = sound
        }
        self.soundButton.isSelected = active
        self.enableButton.isSelected = false
        if let alarmDict = UserDefaults.standard.value(forKey: Resources.getEmailUser() + "alarmDict") as? NSDictionary{
            self.enableButton.isSelected = alarmDict["enabled"] as? Bool ?? false
            self.scheduleSlider.lowerValue = alarmDict["lowerSchedule"] as? Double ?? 9.0
            self.scheduleSlider.upperValue = alarmDict["upperSchedule"] as? Double ?? 18.0
            self.repetitionsSlider.upperValue = alarmDict["repetitions"] as? Double ?? 5.0
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if firstTime{
            firstTime = false
            configureInfo()
        }
    }
    func configureInfo(){
        infoLabel.text = NSLocalizedString("A random alarm will sound so that you stop doing whatever you are doing and reconnect with the present.", comment: "")
        infoMinX = self.infoView.frame.origin.x
        infoWidth = self.infoView.frame.width
        let gradient = CAGradientLayer()
        gradient.frame = self.infoView.bounds
        gradient.colors = [#colorLiteral(red: 0.5333333333, green: 0.5058823529, blue: 0.7764705882, alpha: 1).cgColor, #colorLiteral(red: 0.1058823529, green: 0.6980392157, blue: 0.6745098039, alpha: 1).cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        self.infoView.layer.cornerRadius = 16
        self.infoView.layer.insertSublayer(gradient, at: 0)
        DispatchQueue.main.async {
            self.leftConstraint.constant = self.infoView.frame.maxX
        }
    }
    
    @IBAction func info(sender: UIButton){
        sender.isSelected = !sender.isSelected
        UIView.animate(withDuration: 1, delay: 0.0, options: [], animations: {
            self.leftConstraint.constant = sender.isSelected ? self.infoMinX : self.infoView.frame.maxX
            self.view.layoutIfNeeded()
        }, completion:{ (finished: Bool) in
            
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func disableNotification(notification:Notification){
        /*if self.enableButton.isSelected{
        self.enableButton.isSelected = false
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        }*/
        if self.enableButton.isSelected{
            setNotification(UIButton())
        }
    }
    
    @IBAction func setNotification(_ sender: UIButton){
        sender.isSelected = !sender.isSelected
        if sender == self.soundButton{
            UserDefaults.standard.setValue(self.soundButton.isSelected, forKey: Resources.getEmailUser() + "ReconectionSound")
        }
        let center = UNUserNotificationCenter.current()
        center.removeAllPendingNotificationRequests()
        
        if self.enableButton.isSelected{

            center.getNotificationSettings { (notificationSettings) in
            switch notificationSettings.authorizationStatus {
            case .notDetermined:
                self.requestAuthorization(completionHandler: { (success) in
                    guard success else {
                        DispatchQueue.main.async {
                        self.enableButton.isSelected = false
                        self.disabledDefault()
                        }
                        self.alertOfNotifications()
                        return
                    }
                    
                    // Schedule Local Notification
                    self.scheduleLocalNotification()
                })
            case .authorized:
                // Schedule Local Notification
                self.scheduleLocalNotification()
            case .denied:
                print("Application Not Allowed to Display Notifications")
                DispatchQueue.main.async {
                    self.enableButton.isSelected = false
                    self.disabledDefault()
                }
                self.alertOfNotifications()
                
            case .provisional:
                print("provisional")
            }
        }
        }else{
            
            self.disabledDefault()
            
        }
    }
    func disabledDefault(){
        let alarmDict: NSMutableDictionary = [:]
        alarmDict["enabled"] = false
    }
    func alertOfNotifications(){
        let alertController = UIAlertController(
            title: NSLocalizedString("Notifications are Disabled", comment: ""),
            message: NSLocalizedString("To enable reconnection alarm, you must have Notifications Enabled, go to settings and enable it", comment: ""),
            preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel",comment: ""), style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let openAction = UIAlertAction(title: NSLocalizedString("Open Settings",comment:""), style: .default) { (action) in
            if let url = NSURL(string:UIApplicationOpenSettingsURLString) {
                UIApplication.shared.open(url as URL)
            }
        }
        alertController.addAction(openAction)
        self.present(alertController, animated: true, completion: nil)
    }
    private func requestAuthorization(completionHandler: @escaping (_ success: Bool) -> ()) {
        // Request Authorization
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (success, error) in
            if let error = error {
                print("Request Authorization Failed (\(error), \(error.localizedDescription))")
            }
            
            completionHandler(success)
        }
    }
    
    private func scheduleLocalNotification() {
        // Create Notification Content
        let notificationContent = UNMutableNotificationContent()
        
        // Configure Notification Content
        notificationContent.title = NSLocalizedString("Reconection Alarm", comment: "")
        notificationContent.body = NSLocalizedString("Stop! If you can, close your eyes, inhale deeply and exhale all the air. Repeat until you feel that you are connected to the present. Open your eyes. Be here now", comment: "")
        if self.soundButton.isSelected{
        notificationContent.sound = UNNotificationSound.default()
        }
        let alarmDict: NSMutableDictionary = [:]
        alarmDict["enabled"] = true
        alarmDict["lowerSchedule"] = self.scheduleSlider.lowerValue
        alarmDict["upperSchedule"] = self.scheduleSlider.upperValue
        alarmDict["repetitions"] = self.repetitionsSlider.upperValue
        UserDefaults.standard.setValue(alarmDict, forKey: Resources.getEmailUser() + "alarmDict")
        let randomHour = randomSequenceGenerator(min: Int(scheduleSlider.lowerValue), max: Int(scheduleSlider.upperValue))
        let randomMinutes = randomSequenceGenerator(min: 0, max: 59)
        var savedComponents: [DateComponents] = []
        for i in 0 ..< Int(repetitionsSlider.upperValue){
            let hour = randomHour()
            let minute = hour != Int(scheduleSlider.upperValue) ? randomMinutes() : 0
            var components = createComponents(hour: hour, minute: minute)
            while savedComponents.contains(components){
                let newHour = randomHour()
                let newMinute = newHour != Int(scheduleSlider.upperValue) ? randomMinutes() : 0
                components = createComponents(hour: newHour, minute: newMinute)
            }
            savedComponents.append(components)
            let notificationTrigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: true)
            let notificationRequest = UNNotificationRequest(identifier: "notification\(i)", content: notificationContent, trigger: notificationTrigger)
            UNUserNotificationCenter.current().add(notificationRequest) { (error) in
                if let error = error {
                    print("Unable to Add Notification Request (\(error), \(error.localizedDescription))")
                }
            }
        }

    }
    func createComponents(hour: Int, minute: Int) -> DateComponents{
        
        let components: DateComponents = DateComponents(hour: hour, minute: minute)
        return components
    }
    func randomSequenceGenerator(min: Int, max: Int) -> () -> Int {
        var numbers: [Int] = []
        return {
            if numbers.isEmpty {
                numbers = Array(min ... max)
            }
            
            let index = Int(arc4random_uniform(UInt32(numbers.count)))
            return numbers.remove(at: index)
        }
    }

    @IBAction func back(sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


