//
//  SettingsVC.swift
//  Joy
//
//  Created by Rhonny Gonzalez on 12/6/18.
//  Copyright © 2018 Jesus Nieves. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import NVActivityIndicatorView
import RealmSwift

class SettingsVC: BaseViewController {

    @IBOutlet weak var price7Days: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet var daysButtons: [UIButton]!
    @IBOutlet var starsButtons: [UIButton]!
    @IBOutlet var languageButtons: [UIButton]!
    @IBOutlet weak var askForPasswordButton: UIButton!
    var ref:DatabaseReference!
    var languageSelected = 0
    public var appInstallDate: Date {
        if let documentsFolder = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last {
            if let installDate = try! FileManager.default.attributesOfItem(atPath: documentsFolder.path)[.creationDate] as? Date {
                return installDate
            }
        }
        return Date() // Should never execute
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        configure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let realm = try! Realm()
        let purchased = realm.objects(Purchased.self).first
        if let buyed = purchased {
            self.price7Days.isHidden = true
        } else {
            self.price7Days.isHidden = false
        }
    }
    
    override func viewDidLayoutSubviews() {
        
        self.imgUser.setRounded()
    }
    
    @IBAction func buyPressed(_ sender: Any) {
        let realm = try! Realm()
        let purchased = realm.objects(Purchased.self).first
        if let buyed = purchased {
            let viewController = AppStoryboard.Subscripcion.instance.instantiateViewController(withIdentifier: "SubscripcionVC") as! SubscripcionVC
            self.present(viewController, animated: false)
        } else {
            let viewController = AppStoryboard.Subscripcion.instance.instantiateViewController(withIdentifier: "SubscripcionVC") as! SubscripcionVC
            viewController.showBack = true
            self.present(viewController, animated: false)
        }
    }
    
    @IBAction func askForPassword(sender: UIButton){
        askForPasswordButton.isSelected = !sender.isSelected
        UserDefaults.standard.set(sender.isSelected, forKey: Resources.getEmailUser() + "AskForPassword")
    }
    
    func configure(){
        self.profileNameLabel.text = ""
        
        let defaults = UserDefaults.standard
        let nameUser = defaults.string(forKey: "name")
        let stringURL = defaults.string(forKey: "image")
        if let url = URL(string: stringURL!) {
            self.imgUser.cacheImage(urlString: stringURL!)
        }
        self.profileNameLabel.text = nameUser

        guard let daysOpened = UserDefaults.standard.value(forKey: Resources.getEmailUser() + "DaysOpen") as? Int else{ return }
        for day in daysButtons{
            day.layer.borderColor = UIColor.white.cgColor
            day.layer.borderWidth = 1
            day.layer.cornerRadius = 14
        }
        let daystext = daysOpened == 1 ? NSLocalizedString("day", comment: "") : NSLocalizedString("days", comment: "")
        let title = "\(daysOpened)" + daystext
            daysButtons[0].setTitle(title, for: .normal)
        let installComponents = Calendar.current.dateComponents([.day,.month,.year], from: appInstallDate)
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        let installDate = formatter.date(from: "\(installComponents.day!)/\(installComponents.month!)/\(installComponents.year!) 00:00:00")
        let todayDate = formatter.date(from: "\(Date().preciseLocalDate) 00:00:00")
        var daysInstalled = todayDate!.days(sinceDate: installDate!)
        daysInstalled = daysInstalled! != 0 ? daysInstalled! : 1
        let dividend = daysOpened * 100
        let divisor = daysInstalled! > 0 ? daysInstalled! : 1
        var result = dividend / divisor
        result = result <= 100 ? result : 100
        daysButtons[1].setTitle("\(result)%", for: .normal)
        fillstars(result: result)
        if L102Language.currentAppleLanguage().contains(L102Language.languages()[0]){
            languageButtons[0].backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.3032427226)
            languageSelected = 0
        }else{
            languageButtons[1].backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.3032427226)
            languageSelected = 1
        }
        if let askForPassword = UserDefaults.standard.value(forKey: Resources.getEmailUser() + "AskForPassword"){
            askForPasswordButton.isSelected = askForPassword as! Bool
        }
        askForPasswordButton.setTitle(NSLocalizedString("enabled", comment: ""), for: .selected)
        askForPasswordButton.setTitle(NSLocalizedString("disabled", comment: ""), for: .normal)
    }
    /*
     
 */
    func fillstars(result: Int){
        if result <= 30{
            starsButtons[0].isSelected = true
            return
        }
        if result <= 50{
            for i in 0 ..< starsButtons.count - 3{
                starsButtons[i].isSelected = true
            }
            return
        }
        if result <= 70{
            for i in 0 ..< starsButtons.count - 2{
                starsButtons[i].isSelected = true
            }
            return
        }
        if result <= 90{
            for i in 0 ..< starsButtons.count - 1{
                starsButtons[i].isSelected = true
            }
            return
        }
        if result <= 100{
            for i in 0 ..< starsButtons.count{
                starsButtons[i].isSelected = true
            }
            return
        }
    }
    
    @IBAction func privacyPressed(_ sender: Any) {
        guard let url = URL(string: "https://www.findjoyapp.com/privacy") else {
            return
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    
    @IBAction func termsPressed(_ sender: Any) {
        guard let url = URL(string: "https://www.findjoyapp.com/terms") else {
            return
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func goTo(sender: UIButton){
        var url = ""
        switch sender.tag {
        case 1:
            url = "https://www.findjoyapp.com"
        case 2:
            url = "https://www.instagram.com/findjoy_app/"
        case 3:
            url = "https://www.pinterest.es/findjoyapp/"
        default:
            url = "https://www.facebook.com/findjoyapp/"
        }
        UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
    
    }
    @IBAction func back(sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func switchLanguage(sender: UIButton){
      
        if languageSelected != sender.tag{
            let alertController = UIAlertController(
                title: NSLocalizedString("Alert", comment: ""),
                message: NSLocalizedString("If you change the language, you must restart the app to see results.", comment: ""),
                preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: NSLocalizedString("Ok",comment: ""), style: .default) { (action) in
                
                for i in 0 ..< self.languageButtons.count{
                    
                    if i == sender.tag{
                        
                        self.languageButtons[i].layer.borderWidth = 0
                        self.languageButtons[i].backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.3032427226)
                        L102Language.setAppleLAnguageTo(lang: i)
                        self.languageButtons[i].layer.cornerRadius = 12.5
                        self.languageSelected = i
                    }else{
                        self.languageButtons[i].layer.borderWidth = 1
                        self.languageButtons[i].backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.01)
                        self.languageButtons[i].layer.borderColor = UIColor.white.cgColor
                        self.languageButtons[i].layer.cornerRadius = 12.5
                        self.languageSelected = i
                    }
                }
                
            }
            
            alertController.addAction(okAction)
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
            
        }

        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func logOutPressed(_ sender: Any) {
        try! Auth.auth().signOut()
        self.dismiss(animated: true, completion: nil)
    }
    
}

// constants
let APPLE_LANGUAGE_KEY = "AppleLanguages"
/// L102Language
class L102Language {
    
    class func languages() -> [String]{
        return ["es-","en-"]
    }
    
    /// get current Apple language
    class func currentAppleLanguage() -> String{
        let userdef = UserDefaults.standard
        let langArray = userdef.object(forKey: APPLE_LANGUAGE_KEY) as! NSArray
        let current = langArray.firstObject as! String
        return current
    }
    /// set @lang to be the first in Applelanguages list
    class func setAppleLAnguageTo(lang: Int) {
        let lang = languages()[lang]
        let userdef = UserDefaults.standard
        userdef.set([lang,currentAppleLanguage()], forKey: APPLE_LANGUAGE_KEY)
        userdef.synchronize()
    }
}
