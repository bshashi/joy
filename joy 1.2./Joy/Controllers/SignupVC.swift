//
//  SignupVC.swift
//  Joy
//
//  Created by Jesus Nieves on 29/7/18.
//  Copyright © 2018 Jesus Nieves. All rights reserved.

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage
import NVActivityIndicatorView
import Fusuma

class SignupVC: BaseViewController {
    
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var txtNombre: JoyLoginTextField!
    @IBOutlet weak var txtEmail: JoyLoginTextField!
    @IBOutlet weak var txtPass: JoyLoginTextField!
    @IBOutlet weak var txtPass2: JoyLoginTextField!
    var ref: DatabaseReference!
    var userStorage : StorageReference!
    var userImage: UIImage!
    
    var gender:String!
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        
        let storage = Storage.storage()
        let storageRef = storage.reference()
        userStorage = storageRef.child("users")
        
        gender = "male"
        if let button:UIButton = self.view.viewWithTag(1) as? UIButton {
            button.alpha = 0.5
        }
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name:Notification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name:Notification.Name.UIKeyboardWillHide, object: nil)
    }

    @IBAction func changeImage(sender: UIButton){
        let fusuma = FusumaViewController()
        fusuma.delegate = self
        
        self.present(fusuma, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SegueWelcome"{
            let WVC = segue.destination as! WelcomeVC
            WVC.name = txtNombre.text!
        }
    }
    
    @IBAction func genderPressed(_ sender: UIButton) {
        for index in 1...2 {
            if let button:UIButton = self.view.viewWithTag(index) as? UIButton {
                button.alpha = 1.0
            }
        }
        var tag = sender.tag
        if let button:UIButton = self.view.viewWithTag(tag) as? UIButton {
            button.alpha = 0.5
        }
        switch tag {
        case 2:
            gender = "female"
            break
        default:
            gender = "male"
        }
    }
    
    @IBAction func signUpPressed(_ sender: Any) {
    let valid = validSingUp()
    if valid.0{
    NVActivityIndicatorPresenter.sharedInstance.startAnimating(self.activityData)
        
        Auth.auth().createUser(withEmail: txtEmail.text!, password: txtPass.text!) { (user, error) in
            if ((error) != nil) {
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                self.showAlert(title: "Error", message: error!.localizedDescription)
            } else {
                
                if ((self.userImage) != nil) {
                    let userId: String = "\((user?.uid)!)"
                    let imageRef = self.userStorage.child("\(userId).jpg")
                    let data = UIImageJPEGRepresentation(self.userImage, 0.7)
                    let upload = imageRef.putData(data!, metadata: nil, completion:{ (metadata, error) in
                        if error != nil {
                            print("ha ocurrido un error subiendo la imagen al storage")
                            self.showAlert(title: "Error", message: "Error")
                            return
                        }
                        
                        imageRef.downloadURL(completion: { (url,er) in
                            if er != nil {
                                print("error obteniedo la url de descarga")
                            }
                            
                            if let url = url {
                                let newUser: [String : Any] = ["uid": user?.uid, "name": self.txtNombre.text, "mail": self.txtEmail.text,  "picture": url.absoluteString]
                                self.ref.child("users").child((user?.uid)!).setValue(newUser)
                                
                                let defaults = UserDefaults.standard
                                defaults.set(self.txtNombre.text, forKey: "name")
                                defaults.set(url.absoluteString, forKey: "image")
                                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                                self.performSegue(withIdentifier: "SegueWelcome", sender: self)
                                
                            }
                        })
                    })
                } else {
                    let newUser: [String : Any] = ["uid": user?.uid, "name": self.txtNombre.text, "mail": self.txtEmail.text]
                    self.ref.child("users").child((user?.uid)!).setValue(newUser)
                    
                    let defaults = UserDefaults.standard
                    defaults.set(self.txtNombre.text, forKey: "name")
                    defaults.set("", forKey: "image")
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    self.performSegue(withIdentifier: "SegueWelcome", sender: self)
                }


            }
        }
        }else{
            self.showAlert(title: "Error", message: valid.1)
        }
    }
    
    func validSingUp() -> (Bool,String){
        
        guard txtNombre.text! != "" else{
            return (false,"Name can't be empty.")
        }
        
        guard txtEmail.text! != "" else{
            return (false,"Email can't be empty.")
        }
        guard txtEmail.text!.isValidEmail() else{
            return (false,"The email address is badly formatted.")
        }
        guard txtPass.text! != "" else{
            return (false,"Password can't be empty.")
        }
        guard txtPass.text!.count >= 6 else{
            return (false,"Password must be 6 characters long or more.")
        }
        guard txtPass.text! == txtPass2.text! else{
            return (false,"Passwords must be equals.")
        }
        return (true,"")
    }
    func showAlert(title:String,message:String){
        let alertController = UIAlertController(
            title: NSLocalizedString(title, comment: ""),
            message: NSLocalizedString(message, comment: ""),
            preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: NSLocalizedString("Ok",comment: ""), style: .cancel, handler: nil)
        alertController.addAction(okAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func dismissKeyboard(){
        self.view.endEditing(true)
    }
    
}

extension SignupVC {
    
    @objc func keyboardWillShow(notification:Notification){
        //let frame = firstView.convert(buttons.frame, from:secondView)
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardSize2 = keyboardSize.height
            var maxY: CGFloat = 0.0
            if txtPass.frame.maxY > self.view.frame.size.height - keyboardSize2 && txtPass.isFirstResponder{
                maxY = txtPass.frame.maxY
            }else if txtPass2.frame.maxY > self.view.frame.size.height - keyboardSize2{
                maxY = txtPass2.frame.maxY
            }
            if maxY > self.view.frame.size.height - keyboardSize2{
                self.view.frame.origin.y = self.view.frame.size.height - keyboardSize2 - maxY - 20
            }
            
        }
        
    }
    
    @objc func keyboardWillHide(notification:Notification){
        
        /*  let contentInset:UIEdgeInsets = UIEdgeInsets.zero
         self.scrollV.contentInset = contentInset*/
        self.view.frame.origin.y = 0
    }
    
}

extension SignupVC: FusumaDelegate{
    
    
    // Return the image which is selected from camera roll or is taken via the camera.
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode) {
        profileImg.image = image
        profileImg.layer.cornerRadius = profileImg.frame.height / 2
        profileImg.clipsToBounds = true
        self.userImage = image
        print("Image selected")
    }
    
    // Return the image but called after is dismissed.
    func fusumaDismissedWithImage(_ image: UIImage, source: FusumaMode) {
        print("Called just after FusumaViewController is dismissed.")

    }
    func fusumaVideoCompleted(withFileURL fileURL: URL) {
        print("Called just after a video has been selected.")

    }
    
    // When camera roll is not authorized, this method is called.
    func fusumaCameraRollUnauthorized() {
        
        print("Camera roll unauthorized")
    }
    
    // Return selected images when you allow to select multiple photos.
    func fusumaMultipleImageSelected(_ images: [UIImage], source: FusumaMode) {
        
    }
    // Return an image and the detailed information.
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode, metaData: ImageMetadata) {
        
    }
    
}


