//
//  ObjectivesVC.swift
//  Joy
//
//  Created by Rhonny Gonzalez on 30/5/18.
//  Copyright © 2018 Jesus Nieves. All rights reserved.
//

import UIKit

class ObjectivesVC: UIViewController {

    @IBOutlet weak var shortTermView: UIView!
    @IBOutlet weak var longTermView: UIView!
    @IBOutlet weak var shortTermTV: UITableView!
    @IBOutlet weak var longTermTV: UITableView!
    @IBOutlet weak var shortTermHeight: NSLayoutConstraint!
    @IBOutlet weak var longTermHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var shortLbl: UILabel!
    @IBOutlet weak var longLbl: UILabel!
    
    var shortTermFinished: [Bool] = []
    var longTermFinished: [Bool] = []
    var shortTermArray: [[String:Any]] = []
    var longTermArray: [[String:Any]] = []
    
    var min = 4
    var isDeleting = false
    var maxY: CGFloat = 0.0
    var tap: UITapGestureRecognizer!
    var shortEmpty = false
    var longEmpty = false
    
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var leftConstraint: NSLayoutConstraint!
    
    var infoMinX = CGFloat(0)
    var infoWidth = CGFloat(0)
    var firstTime = true
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
        configure()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.view.frame.size.height < 600{
            self.shortTermHeight.constant = 140
            self.longTermHeight.constant = 140
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if firstTime{
            firstTime = false
            configureInfo()
        }
    }
    func configureInfo(){
        infoLabel.text = NSLocalizedString("Tap the lists to write your goals. Put a tick as you go fulfilling them.", comment: "")
        infoMinX = self.infoView.frame.origin.x
        infoWidth = self.infoView.frame.width
        let gradient = CAGradientLayer()
        gradient.frame = self.infoView.bounds
        gradient.colors = [#colorLiteral(red: 0.5333333333, green: 0.5058823529, blue: 0.7764705882, alpha: 1).cgColor, #colorLiteral(red: 0.1058823529, green: 0.6980392157, blue: 0.6745098039, alpha: 1).cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        self.infoView.layer.cornerRadius = 16
        self.infoView.layer.insertSublayer(gradient, at: 0)
        DispatchQueue.main.async {
            self.leftConstraint.constant = self.infoView.frame.maxX
        }
    }
    
    @IBAction func info(sender: UIButton){
        sender.isSelected = !sender.isSelected
        UIView.animate(withDuration: 1, delay: 0.0, options: [], animations: {
            self.leftConstraint.constant = sender.isSelected ? self.infoMinX : self.infoView.frame.maxX
            self.view.layoutIfNeeded()
        }, completion:{ (finished: Bool) in
            
        })
    }
    func configure(){
        if let shortTerm = UserDefaults.standard.value(forKey: Resources.getEmailUser() + "ShortTermItems") as? [[String:Any]]{
            shortTermArray = shortTerm
        }
        if let longTerm = UserDefaults.standard.value(forKey: Resources.getEmailUser() + "LongTermItems") as? [[String:Any]]{
            longTermArray = longTerm
        }
        shortTermFinished = [false,false,false,false]
        shortTermTV.delegate = self
        shortTermTV.dataSource = self
        longTermTV.delegate = self
        longTermTV.dataSource = self
        shortLbl.text = NSLocalizedString("Short Term", comment: "")
        longLbl.text = NSLocalizedString("Long Term", comment: "")
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name:Notification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name:Notification.Name.UIKeyboardWillHide, object: nil)
        tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissScreen))
        //THIS CODE IS FOR SOMETHING IMPORTANT, DONT REMOVE IT XD
        self.view.addGestureRecognizer(tap)
        self.view.removeGestureRecognizer(tap)
        //END OF IMPORTANT CODE.
    }
    @IBAction func back(sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func deleteItems(sender: UIButton){
        self.isDeleting = !self.isDeleting
        if !self.isDeleting{
            shortTermTV.reloadData()
            longTermTV.reloadData()
            return
        }
        var short = 0
        var long = 0
        for i in 0 ..< shortTermArray.count{
            if shortTermArray[i]["item"] as? String ?? "" != ""{
                shortTermTV.reloadData()
                break
            }else{
                short += 1
            }
        }
        for i in 0 ..< longTermArray.count{
            if longTermArray[i]["item"] as? String ?? "" != ""{
                longTermTV.reloadData()
                break
            }else{
                long += 1
            }
        }
        if shortTermArray.count == short && longTermArray.count == long{
            self.isDeleting = false
            self.showaAlert(title: "Alert", message: "There is no objectives to delete.")
        }
        
    }
    @IBAction func scrollTV(sender: UIButton){
        if !(self.view.gestureRecognizers?.contains(tap))!{
        switch sender.tag {
        case 0:
            let indexPathRow = shortTermTV.indexPathsForVisibleRows?.first!.row
            if indexPathRow! != 0{
                
                let indexPath = IndexPath(row: indexPathRow! - 1, section: 0)
                shortTermTV.scrollToRow(at: indexPath, at: .top, animated: true)
                
            }
        case 1:
            let indexPathRow = longTermTV.indexPathsForVisibleRows?.first!.row
            if indexPathRow! != 0{
                
                let indexPath = IndexPath(row: indexPathRow! - 1, section: 0)
                longTermTV.scrollToRow(at: indexPath, at: .top, animated: true)
                
            }
        case 2:
            let indexPathRow = shortTermTV.indexPathsForVisibleRows?.last!.row
            if indexPathRow! + 1 == shortTermArray.count{
                for shortItem in shortTermArray{
                    if shortItem["item"] as? String ?? "" == ""{
                        self.showaAlert(title: "Alert", message: "You must fill all items to insert a new one.")
                        return
                    }
                }
                shortTermArray.append(["item":"","finished":false])
                shortTermTV.beginUpdates()
                shortTermTV.insertRows(at: [IndexPath(row: shortTermArray.count-1, section: 0)], with: .automatic)
                shortTermTV.endUpdates()
            }
            let indexPath = IndexPath(row: indexPathRow! + 1, section: 0)
            shortTermTV.scrollToRow(at: indexPath, at: .bottom, animated: true)
        case 3:
            let indexPathRow = longTermTV.indexPathsForVisibleRows?.last!.row
            if indexPathRow! + 1 == longTermArray.count{
                for longItem in longTermArray{
                    if longItem["item"] as? String ?? "" == ""{
                        self.showaAlert(title: "Alert", message: "You must fill all items to insert a new one.")
                        return
                    }
                }
                longTermArray.append(["item":"","finished":false])
                longTermTV.beginUpdates()
                longTermTV.insertRows(at: [IndexPath(row: longTermArray.count-1, section: 0)], with: .automatic)
                longTermTV.endUpdates()
            }
            let indexPath = IndexPath(row: indexPathRow! + 1, section: 0)
            longTermTV.scrollToRow(at: indexPath, at: .bottom, animated: true)
            
            
        default:
            break
        }
        }else{
            dismissScreen()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ObjectivesVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == shortTermTV{
            if shortTermArray.count < min{
                for _ in shortTermArray.count ..< min{
                    shortTermArray.append(["item":"","finished":false])
                }
            }
            return shortTermArray.count
        }
        if longTermArray.count < min{
            for _ in longTermArray.count ..< min{
                longTermArray.append(["item":"","finished":false])
            }
        }
        return longTermArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ObjectivesCell", for: indexPath) as! ObjectivesTVCell
        cell.contentView.layer.cornerRadius = 4
        cell.contentView.layer.borderWidth = 1
        cell.contentView.layer.borderColor = UIColor.lightGray.cgColor
        let item = tableView == shortTermTV ? shortTermArray[indexPath.row]["item"] as? String ?? "" : longTermArray[indexPath.row]["item"] as? String ?? ""
        let finished = tableView == shortTermTV ? shortTermArray[indexPath.row]["finished"] as? Bool ?? false : longTermArray[indexPath.row]["finished"] as? Bool ?? false
        let value = tableView == shortTermTV ? 999 : -999
        cell.textField.delegate = self
        cell.textField.tag = indexPath.row + value
        //cell.textField.attributedPlaceholder = NSAttributedString(string:NSLocalizedString("Type something...", comment: ""))
        // Define paragraph styling

        let paraStyle = NSMutableParagraphStyle()
        paraStyle.firstLineHeadIndent = 8.0
        
        // Apply paragraph styles to paragraph
        cell.textField.allowsEditingTextAttributes = true
        cell.textField.defaultTextAttributes = [NSAttributedStringKey.paragraphStyle.rawValue : paraStyle,
             NSAttributedStringKey.font.rawValue: UIFont(name: "Ecosmith Script", size: 20.0)]
        let attr = [NSAttributedStringKey.paragraphStyle : paraStyle,
        NSAttributedStringKey.font: UIFont(name: "Ecosmith Script", size: 20.0)] as [NSAttributedStringKey: Any]
        cell.textField.attributedText = NSAttributedString(string: item)
        cell.textField.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("Type something...", comment: ""), attributes: attr)
        
        if isDeleting{
            if cell.textField.text != ""{
                cell.deleteWidth.constant = 20
            }else{
                cell.deleteWidth.constant = 0
            }
            cell.textField.isUserInteractionEnabled = false
        }else{
            cell.deleteWidth.constant = 0
            cell.textField.isUserInteractionEnabled = true
            cell.textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        }
        cell.checkButton.isUserInteractionEnabled = false
        if finished{
            cell.checkButton.isSelected = true
            return cell
        }
        cell.checkButton.isSelected = false
        return cell
        
//        let cell = tableView.dequeueReusableCell(withIdentifier: "LongTermCell", for: indexPath)
//        let textField = cell.viewWithTag(1) as! UITextField
//        let checkButton = cell.viewWithTag(2) as! UIButton
//        let item = shortTermArray[indexPath.row]["item"] as? String ?? ""
//        let finished = shortTermArray[indexPath.row]["finished"] as? Bool ?? false
//        textField.text = item
//        checkButton.isUserInteractionEnabled = false
//        if finished{
//            checkButton.isSelected = true
//            return cell
//        }
//        checkButton.isSelected = false
//        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let validCell = tableView.cellForRow(at: indexPath) as? ObjectivesTVCell else {return}
        if isDeleting{
            if validCell.deleteWidth.constant == 20{
            if tableView == shortTermTV{
                shortTermArray.remove(at: indexPath.row)
                if shortTermArray.count < min{
                    shortTermArray.append(["item":"","finished":false])
                    UserDefaults.standard.setValue(shortTermArray,forKey: Resources.getEmailUser() + "ShortTermItems")
                    shortTermTV.reloadData()
                    return
                }
                UserDefaults.standard.setValue(shortTermArray,forKey: Resources.getEmailUser() + "ShortTermItems")
                shortTermTV.deleteRows(at: [indexPath], with: .automatic)
                return
            }
            longTermArray.remove(at: indexPath.row)
            if longTermArray.count < min{
                longTermArray.append(["item":"","finished":false])
                longTermTV.reloadData()
                UserDefaults.standard.setValue(longTermArray,forKey: Resources.getEmailUser() + "LongTermItems")
                return
            }
            UserDefaults.standard.setValue(longTermArray,forKey: Resources.getEmailUser() + "LongTermItems")
            longTermTV.deleteRows(at: [indexPath], with: .automatic)
            return
            }
            return
        }
        if tableView == shortTermTV{
            let item = validCell.textField.text ?? ""//shortTermArray[indexPath.row]["item"] as? String ?? ""
            let finished = !validCell.checkButton.isSelected//!(shortTermArray[indexPath.row]["finished"] as! Bool)
            let newItem = ["item":item,"finished":finished] as [String : Any]
            shortTermArray[indexPath.row] = newItem
            UserDefaults.standard.setValue(shortTermArray,forKey: Resources.getEmailUser() + "ShortTermItems")
            shortTermTV.reloadRows(at: [indexPath], with: .none)
            return
        }
        
        let item = validCell.textField.text ?? ""//longTermArray[indexPath.row]["item"] as? String ?? ""
        let finished = !validCell.checkButton.isSelected//!(longTermArray[indexPath.row]["finished"] as! Bool)
        let newItem = ["item":item,"finished":finished] as [String : Any]
        longTermArray[indexPath.row] = newItem
        longTermTV.reloadRows(at: [indexPath], with: .none)
        UserDefaults.standard.setValue(longTermArray,forKey: Resources.getEmailUser() + "LongTermItems")

    }
    
    func showaAlert(title: String, message: String){
        let alert = UIAlertController(title: NSLocalizedString(title, comment: ""), message: String(format: NSLocalizedString(message,comment: "")), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

extension ObjectivesVC: UITextFieldDelegate{

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        let frame = view.convert(textField.frame, from:textField.superview!)
        print (frame)
        //maxY = frame.maxY
        if textField.tag >= 999{
            maxY = self.shortTermView.frame.maxY
        }else{
            let frame = view.convert(longTermView.frame, from:longTermView.superview!)
            maxY = frame.maxY
        }
        self.view.addGestureRecognizer(tap)
        if textField.tag >= 999{
            self.longTermView.isUserInteractionEnabled = false
        }else{
            self.shortTermView.isUserInteractionEnabled = false
        }
        return true
        
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if textField.tag >= 999{
            let index = textField.tag - 999
            let item = textField.text ?? ""
            let finished = (shortTermArray[index]["finished"] as! Bool)
            let newItem = ["item":item,"finished":finished] as [String : Any]
            shortTermArray[index] = newItem
            UserDefaults.standard.setValue(shortTermArray,forKey: Resources.getEmailUser() + "ShortTermItems")
            return
        }
        
        let index = textField.tag + 999
        let item = textField.text ?? ""
        let finished = (longTermArray[index]["finished"] as! Bool)
        let newItem = ["item":item,"finished":finished] as [String : Any]
        longTermArray[index] = newItem
        UserDefaults.standard.setValue(longTermArray,forKey: Resources.getEmailUser() + "LongTermItems")
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.tag >= 999{
            let index = textField.tag - 999
            let item = textField.text ?? ""
            let finished = (shortTermArray[index]["finished"] as! Bool)
            let newItem = ["item":item,"finished":finished] as [String : Any]
            shortTermArray[index] = newItem
            UserDefaults.standard.setValue(shortTermArray,forKey: Resources.getEmailUser() + "ShortTermItems")
            return
        }
        
        let index = textField.tag + 999
        let item = textField.text ?? ""
        let finished = (longTermArray[index]["finished"] as! Bool)
        let newItem = ["item":item,"finished":finished] as [String : Any]
        longTermArray[index] = newItem
        UserDefaults.standard.setValue(longTermArray,forKey: Resources.getEmailUser() + "LongTermItems")

    }

    @objc func keyboardWillShow(notification:Notification){
        //let frame = firstView.convert(buttons.frame, from:secondView)
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardSize2 = keyboardSize.height
            if maxY > self.view.frame.size.height - keyboardSize2{
                self.view.frame.origin.y = self.view.frame.size.height - keyboardSize2 - maxY - 20
            }
            
        }
        
    }
    
    @objc func keyboardWillHide(notification:Notification){
        
        /*  let contentInset:UIEdgeInsets = UIEdgeInsets.zero
         self.scrollV.contentInset = contentInset*/
        self.view.frame.origin.y = 0
    }
    
    @objc func dismissScreen(){
        self.view.removeGestureRecognizer(tap)
        self.shortTermView.isUserInteractionEnabled = true
        self.longTermView.isUserInteractionEnabled = true
        self.view.endEditing(true)
    }
}

