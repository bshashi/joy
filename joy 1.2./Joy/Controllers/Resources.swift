//
//  Resources.swift
//  Joy
//
//  Created by Rhonny Gonzalez on 16/8/18.
//  Copyright © 2018 Jesus Nieves. All rights reserved.
//

import UIKit
import FirebaseAuth

class Resources: NSObject {

    class func getEmailUser() -> String{
        let email = Auth.auth().currentUser?.email ?? ""
        return email
    }
    
    class func getProposals() -> NSArray{
        return [
            ["imageName":"1.png","text-en":"Start a new book","text-es":"Empieza un libro"],
            ["imageName":"2.png","text-en":"Take a walk on the beach","text-es":"Da un paseo por la playa"],
            ["imageName":"3.png","text-en":"Call your best friend","text-es":"Llama a tu mejor amigo"],
            ["imageName":"4.png","text-en":"Have a coffee with your mother","text-es":"T\u{F3}mate un caf\u{E9} con tu madre"],
            ["imageName":"5.png","text-en":"Get rid of what you don’t need","text-es":"Deshazte de aquello que no necesites"],
            ["imageName":"6.png","text-en":"Sunbathe 30 minutes","text-es":"Toma el sol 30 minutos"],
            ["imageName":"7.png","text-en":"Take a relaxing bath","text-es":"Date un ba\u{F1}o relajante"],
            ["imageName":"8.png","text-en":"Get a beauty treatment","text-es":"Hazte un tratamiento"],
            ["imageName":"9.png","text-en":"Dance like mad","text-es":"Baila como un loco"],
            ["imageName":"10.png","text-en":"Move, run, sweat","text-es":"Mu\u{E9}vete, corre, suda"],
            ["imageName":"11.png","text-en":"Do some yoga","text-es":"Practica yoga"],
            ["imageName":"12.png","text-en":"Cook something tasty and healthy","text-es":"Cocina algo sano y rico"],
            ["imageName":"13.png","text-en":"Watch an inspiring video","text-es":"Mira un video inspirador"],
            ["imageName":"14.png","text-en":"Get lost in the forest","text-es":"Pi\u{E9}rdete en el bosque"],
            ["imageName":"15.png","text-en":"Help someone selflessly","text-es":"Ayuda a alguien desinteresadamente"],
            ["imageName":"16.png","text-en":"Do something for your community","text-es":"Haz algo por tu comunidad"],
            ["imageName":"17.png","text-en":"Plan a trip","text-es":"Planifica un viaje"],
            ["imageName":"18.png","text-en":"Play music out loud","text-es":"Ponte m\u{FA}sica a todo volumen"],
            ["imageName":"19.png","text-en":"Enjoy your favorite movie","text-es":"Disfruta de tu pel\u{ED}cula favorita"],
            ["imageName":"20.png","text-en":"Air out your house","text-es":"Ventila tu casa"],
            ["imageName":"21.png","text-en":"Finish what you’ve left to do","text-es":"Acaba aquello que te queda por hacer"],
            ["imageName":"22.png","text-en":"Write your thoughts","text-es":"Escribe tus pensamientos"],
            ["imageName":"23.png","text-en":"Sit on the grass","text-es":"Si\u{E9}ntate en la hierba"],
            ["imageName":"24.png","text-en":"Savor the food with your eyes closed","text-es":"Saborea la comida con los ojos cerrados"],
            ["imageName":"25.png","text-en":"Get a massage","text-es":"Date un masaje"],
            ["imageName":"26.png","text-en":"Smile, just because","text-es":"Sonr\u{ED}e, porque s\u{ED}"],
            ["imageName":"27.png","text-en":"Learn something new","text-es":"Aprende algo nuevo"],
            ["imageName":"28.png","text-en":"Say I‘m Sorry","text-es":"Pide perd\u{F3}n"],
            ["imageName":"29.png","text-en":"Say I love you”","text-es":"D\u{ED} \"te quiero\""],
            ["imageName":"30.png","text-en":"Do something creative","text-es":"Haz algo creativo"],
            ["imageName":"31.png","text-en":"Play","text-es":"Juega"],
            ["imageName":"32.png","text-en":"Organize your budget","text-es":"Organiza tus finanzas"],
            ["imageName":"33.png","text-en":"Take a restful nap","text-es":"\u{E9}chate una siesta reparadora"],
            ["imageName":"34.png","text-en":"Reread your favorite book","text-es":"Relee tu libro favorito"],
            ["imageName":"35.png","text-en":"Dance in the rain","text-es":"Baila bajo la lluvia"],
            ["imageName":"36.png","text-en":"Say something nice to a stranger","text-es":"D\u{ED} algo agradable a un desconocido"],
            ["imageName":"37.png","text-en":"Hug your pet","text-es":"Abraza a tu mascota"],
            ["imageName":"38.png","text-en":"Leave a bad habit","text-es":"Deja un mal h\u{E1}bito"],
            ["imageName":"39.png","text-en":"Smile yourself in the mirror","text-es":"Sonr\u{ED}ete a ti mismo en el espejo"],
            ["imageName":"40.png","text-en":"Organize your electronic devices","text-es":"Organiza tus dispositivos electr\u{F3}nicos"],
            ["imageName":"41.png","text-en":"Go somewhere new","text-es":"Ve a un sitio desconocido"],
            ["imageName":"42.png","text-en":"Change the sheets","text-es":"Pon s\u{E1}banas limpias"],
            ["imageName":"43.png","text-en":"Make a gift","text-es":"Haz un regalo"],
            ["imageName":"44.png","text-en":"Write a letter forgiving someone","text-es":"Escribe una carta perdonando a alguien"],
            ["imageName":"45.png","text-en":"Help an NGO","text-es":"Ayuda a una ONG"],
            ["imageName":"46.png","text-en":"Redecorate your home","text-es":"Redecora tu casa"],
            ["imageName":"47.png","text-en":"Turn off your phone","text-es":"Apaga tu tel\u{E9}fono"],
            ["imageName":"48.png","text-en":"Get up early to see the sunrise","text-es":"Lev\u{E1}ntate para ver el amanecer"],
            ["imageName":"49.png","text-en":"Go to your favorite place to watch the sunset","text-es":"Ve a tu sitio favorito para ver el atardecer"],
            ["imageName":"50.png","text-en":"Give yourself a treat","text-es":"Date un capricho"]
        ]
    }
    
    class func getInspiration() -> NSArray{
        
        return [
            ["text-en":"If you’re dreams don’t scare you, they are not big enough.","text-es":"Si tus sue\u{F1}os no te asustan es que no son suficientemente grandes.","Author":""],
            ["text-en":"If you get tired, learn to rest, not to quit.","text-es":"Si te sientes abrumado, aprende a descansar, no renuncies.","Author":"Banksy"],
            ["text-en":"You are not a drop in the ocean. You are the entire ocean in a drop.","text-es":"No eres una gota de agua en el oc\u{E9}ano, eres el oc\u{E9}ano entero en una gota.","Author":""],
            ["text-en":"-What If I fall?\n-Oh, darling, what if you fly?","text-es":"-¿Y si me caigo?\n-Oh cari\u{F1}o, ¿y si echas a volar?","Author":""],
            ["text-en":"Life begins at the end of your comfort zone.","text-es":"Tu vida comienza d\u{F3}nde acaba tu zona de confort.","Author":""],
            ["text-en":"The journey isn’t about becoming a different person, but loving who you are right now.","text-es":"El camino no es sobre c\u{F3}mo convertirte en otra persona, si no de amar a la persona que ya eres.","Author":""],
            ["text-en":"People wait all week for Friday, all year for summer, all life for happiness.","text-es":"Las personas esperan toda la semana para que llegue el viernes, todo el a\u{F1}o para que llegue el verano, toda la vida para que llegue la Felicidad.","Author":""],
            ["text-en":"Stop saying yes to s*** you hate.","text-es":"Deja de decir \"s\u{ED}\" a la m*** que odias.","Author":""],
            ["text-en":"namaste.\nMy soul honors your soul. I honor the place in you where the entire universe resides. I honor the light, love, truth, beauty & peace within you, because it is also within me. In sharing these things we are united, we are the same, we are one.","text-es":"namast\u{E9}.\nMi alma honra tu alma. Honro el lugar en ti donde reside el universo entero. Honro la luz, el amor, la verdad, la belleza y la paz que est\u{E1}n en ti, porque est\u{E1}n tambi\u{E9}n en m\u{ED}. Al compartir estas cosas, estamos unidos, somos lo mismo, somos uno.","Author":""],
            ["text-en":"I am enough. Who I am is enough. What I do is enough, and what I have is enough.","text-es":"Yo soy suficiente. Qui\u{E9}n soy es suficiente. Lo que hago es suficiente, y lo que tengo es suficiente.","Author":""],
            ["text-en":"Inhale confidence, exhale doubt.","text-es":"Inhala confianza, exhala dudas.","Author":""],
            ["text-en":"Feel the fear and do it anyway.","text-es":"Siente el miedo y hazlo de todas formas.","Author":""],
            ["text-en":"I give myself permission to slow down.","text-es":"Me doy permiso para ir m\u{E1}s despacio.","Author":""],
            ["text-en":"Be the energy you want to attract.","text-es":"S\u{E9} la energ\u{ED}a que quieres atraer.","Author":""],
            ["text-en":"Sky above, earth below, peace within.","text-es":"Cielo sobre mi cabeza, Tierra bajo mis pies, paz en mi interior.","Author":""],
            ["text-en":"Everything I need is within me.","text-es":"Todo lo que siempre necesit\u{E9}, necesito y necesitar\u{E9} est\u{E1} dentro de m\u{ED}.","Author":""],
            ["text-en":"Just in case no one told you today: Hello. Good morning. You’re doing great. I believe in you. Nice butt.","text-es":"Por si acaso nadie te lo dijo hoy: Hola. Buenos d\u{ED}as. Lo est\u{E1}s haciendo genial. Creo en ti. Bonito trasero.","Author":""],
            ["text-en":"Do more of what makes you happy.","text-es":"Haz m\u{E1}s de aquello que te hace feliz.","Author":""],
            ["text-en":"I choose love.\nI choose peace.\nI choose clarity.\nI choose rawness.\nI choose honesty.\nI choose happiness.\nI choose kindness.\nI choose myself.","text-es":"Elijo amor.\nElijo paz.\nElijo claridad.\nElijo rigor.\nElijo honestidad.\nElijo felicidad.\nElijo amabilidad.\nMe elijo a m\u{ED}.","Author":""],
            ["text-en":"You are not a mess. You are a feeling person in a messy world.","text-es":"No eres un desastre. Eres una persona con sentimientos en un mundo desastroso.","Author":""],
            ["text-en":"In a society that profits from your self- doubt, liking yourself is a rebellious act.","text-es":"En una sociedad que se beneficia de tus inseguridades, gustarte a ti mismo es un acto de rebeld\u{ED}a.","Author":""],
            ["text-en":"Our brain constructs our reality based on what we pay attention to. When you learn to manage and direct your focus, you have the power to improve practically all aspects of your life.","text-es":"Nuestros cerebro construye nuestra realidad en base a lo que prestamos atenci\u{F3}n. Cuando aprendes a administrar y dirigir su enfoque, tienes el poder de mejorar pr\u{E1}cticamente todos los aspectos de su vida.","Author":""],
            ["text-en":"The only thing that stands between people and their dreams is the fear to fail.","text-es":"Lo \u{FA}nico que se interpone entre la gente y sus sue\u{F1}os es el miedo al fracaso.","Author":"Robin Sharma"],
            ["text-en":"Self-control is the DNA of the domain of life.","text-es":"El autodominio es el ADN del dominio de la vida.","Author":"Robin Sharma"],
            ["text-en":"Dare to appeal to your greatness. It is your natural right.","text-es":"Atr\u{E9}vete a apelar a tu grandeza. Es tu derecho natural.","Author":"Robin Sharma"],
            ["text-en":"The goals give life vigor.","text-es":"Las metas dan vigor a la vida.","Author":"Robin Sharma"],
            ["text-en":"The universe favors the brave. When you decide to elevate your soul to its highest level, the strength of your soul will guide you to a magical place full of valuable treasures.","text-es":"El universo favorece a los valientes. Cuando decidas elevar tu alma a su m\u{E1}s alto nivel, la fuerza de tu alma te guiar\u{E1} a un lugar m\u{E1}gico repleto de valiosos tesoros.","Author":"Robin Sharma"],
            ["text-en":"Letting go is medicine that heals the heart.","text-es":"Dejar ir es la medicina que sana tu coraz\u{F3}n.","Author":"Yung Pueblo"],
            ["text-en":"Forgiveness is the key to happiness.","text-es":"El perd\u{F3}n es la llave de la felicidad.","Author":""],
            ["text-en":"Do no harm, but take no s***.","text-es":"No hagas da\u{F1}o, pero no permitas m*** de otros.","Author":""],
            ["text-en":"Ask the universe for what you want. If you don’t know what you want in life, how are you ever going to create the life of your dreams? Write down everything you want to create. Be detailed. Take all the active steps you need to take. Ask for what you want and keep your intentions loving and clear.","text-es":"Pide al universo lo que quieres. Si no sabes lo que quieres en la vida, ¿c\u{F3}mo vas a crear la vida de tus sue\u{F1}os? Escribe todo lo que quieres crear. Se detallado. Toma todos los pasos que necesitas tomar. Pide lo que quieres y mant\u{E9}n tus intenciones claras.","Author":"Rachel Brathen"],
            ["text-en":"Meditate, meditate, and keep meditating until you’ve learned to let go of all the things you fear to lose.","text-es":"Medita, medita y sigue meditando hasta que hayas aprendido a dejar ir aquello que temes perder.","Author":"Rachel Brathen"],
            ["text-en":"Concentrate on what your body does for you, not what it looks like.","text-es":"Presta atenci\u{F3}n a lo que tu cuerpo hace por ti, no en c\u{F3}mo se ve.","Author":"Rachel Brathen"],
            ["text-en":"Nothing is more important than your own well-being. Sometimes saying no is caring for yourself.","text-es":"Nada es mas importante que tu propio bienestar. A veces decir “no” es cuidar de ti mismo.","Author":""],
            ["text-en":"Love what is. Don't judge. Don’t label. Love. Just Love.","text-es":"Ama lo que es. No judges. No pongas etiquetas. Ama. S\u{F3}lo ama.","Author":"Rachel Brathen"],
            ["text-en":"If it can be solved, there’s no need to worry, and if it can’t be solved, worry is of no use.","text-es":"Si tiene soluci\u{F3}n, no hay necesidad de preocupaci\u{F3}n. Si no tiene soluci\u{F3}n, la preocupaci\u{F3}n es in\u{FA}til.","Author":"Dalai Lama"],
            ["text-en":"Do not let the behaviour of others destroy your inner peace.","text-es":"No permitas que el comportamiento de otros destruya tu paz interior.","Author":"Dalai Lama"],
            ["text-en":"If you don’t love yourself, you cannot love others.","text-es":"Si no te amas a ti mismo, no puedes amar a otros.","Author":"Dalai Lama"],
            ["text-en":"Actually, you can.","text-es":"En realidad, s\u{ED} puedes.","Author":""],
            ["text-en":"You know? You're at the right time and place.","text-es":"¿Sabes? Estas en el momento y lugar adecuados.","Author":""],
            ["text-en":"One of the keys to happiness is not expecting others to be perfect.","text-es":"Una de las claves de la felicidad es no esperar que otros sean perfectos.","Author":"Yung Pueblo"],
            ["text-en":"Your self love is a medicine for the earth.","text-es":"Tu amor propio es medicina para la Tierra.","Author":"Yung Pueblo"],
            ["text-en":"I choose to stay hopeful when I experience low frequencies and stay humble when I am on top of the world emanating at my highest state.","text-es":"Elijo mantener la esperanza cuando experimente malos momentos y me mantenerme humilde cuando est\u{E9} en la cima del mundo.","Author":"Bee Bosnak"]
        ]
        
    }
    
}
