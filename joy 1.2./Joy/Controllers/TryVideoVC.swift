//
//  TryVideoVC.swift
//  Joy
//
//  Created by Rhonny Gonzalez on 9/6/18.
//  Copyright © 2018 Jesus Nieves. All rights reserved.
//
import UIKit
import AVFoundation
import AVKit

class TryVideoVC: UIViewController {
    
    
    @IBOutlet weak var viewVideo: UIView!
    @IBOutlet weak var stackButtons: UIStackView!
    @IBOutlet var musicsButtons: [UIButton]!
    @IBOutlet weak var soundButton: UIButton!
    @IBOutlet weak var inhExhLabel: UILabel!
    
    var avPlayer: AVPlayer!
    var playerMusic: AVAudioPlayer!
    var playerGong: AVAudioPlayer!
    var avpController = AVPlayerViewController()
    let transparentVideoView = JoyVideoView()
    var gongTimer: Timer!
    var finishTimer: Timer!
    var timeToLast = 1
    var isHere = true
    
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var leftConstraint: NSLayoutConstraint!
    
    var infoMinX = CGFloat(0)
    var infoWidth = CGFloat(0)
    var firstTime = true
    var buttonSelected = [false,false,false]
    override func viewDidLoad() {
        
        super.viewDidLoad()
        transparentVideoView.backgroundColor = .clear
        viewVideo.backgroundColor = .clear
        transparentVideoView.frame = viewVideo.bounds
        transparentVideoView.configure(fileName: "video", ext: "mov")
            viewVideo.addSubview(transparentVideoView)
        transparentVideoView.play()
        self.playSound(sound: "inh-exh")
        gongTimer = Timer.scheduledTimer(timeInterval: 8.2, target: self, selector: #selector(gongSound), userInfo: nil, repeats: true)
        finishTimer = Timer.scheduledTimer(timeInterval: Double(timeToLast) * 60.0, target: self, selector: #selector(finish), userInfo: nil, repeats: false)
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.transparentVideoView.player.currentItem, queue: .main) { _ in
            print("finalizo")
            self.transparentVideoView.player.seek(to: kCMTimeZero)
            self.transparentVideoView.play()
        }
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if firstTime{
            firstTime = false
            configureInfo()
        }
    }
    func configureInfo(){
        infoLabel.text = NSLocalizedString("Sit with a long spine, in a comfortable position, and focus on your breath. If you get distracted, just come back.", comment: "")
        infoMinX = self.infoView.frame.origin.x
        infoWidth = self.infoView.frame.width
        let gradient = CAGradientLayer()
        gradient.frame = self.infoView.bounds
        gradient.colors = [#colorLiteral(red: 0.5333333333, green: 0.5058823529, blue: 0.7764705882, alpha: 1).cgColor, #colorLiteral(red: 0.1058823529, green: 0.6980392157, blue: 0.6745098039, alpha: 1).cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        self.infoView.layer.cornerRadius = 16
        self.infoView.layer.insertSublayer(gradient, at: 0)
        DispatchQueue.main.async {
            self.leftConstraint.constant = self.infoView.frame.maxX
        }
    }
    
    @IBAction func info(sender: UIButton){
        sender.isSelected = !sender.isSelected
        UIView.animate(withDuration: 1, delay: 0.0, options: [], animations: {
            self.leftConstraint.constant = sender.isSelected ? self.infoMinX : self.infoView.frame.maxX
            self.view.layoutIfNeeded()
        }, completion:{ (finished: Bool) in
            
        })
    }
    
    @objc func finish(){
        isHere = false
        self.transparentVideoView.stop()
        finishTimer.invalidate()
        finishTimer = nil
        if playerGong != nil{
            gongTimer.invalidate()
            gongTimer = nil
            playerGong.stop()
        }
        if playerMusic != nil{
            playerMusic.stop()
        }
        self.performSegue(withIdentifier:"SegueFinish", sender: nil)
    }
    @objc func gongSound(){
        inhExhLabel.text = inhExhLabel.text == NSLocalizedString("Inhale", comment: "") ? NSLocalizedString("Exhale", comment: "") : NSLocalizedString("Inhale", comment: "")
        self.playSound(sound: "inh-exh")
    }
    @IBAction func selectSound(sender: UIButton){
        
            switch sender {
            case musicsButtons[0]:
                buttonSelected[0] = !buttonSelected[0]
                buttonSelected[1] = false
                buttonSelected[2] = false
                musicsButtons[1].tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                musicsButtons[2].tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                if buttonSelected[0]{
                    musicsButtons[0].tintColor = #colorLiteral(red: 0.4078431373, green: 0.4078431373, blue: 0.4078431373, alpha: 1)
                    playSound(sound: "water")
                }else{
                    musicsButtons[0].tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                    playSound(sound: "")
                }
                
            case musicsButtons[1]:
                buttonSelected[0] = false
                buttonSelected[1] = !buttonSelected[1]
                buttonSelected[2] = false
                musicsButtons[0].tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                musicsButtons[2].tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                if buttonSelected[1]{
                    musicsButtons[1].tintColor = #colorLiteral(red: 0.4078431373, green: 0.4078431373, blue: 0.4078431373, alpha: 1)
                    playSound(sound: "leave")
                }else{
                    musicsButtons[1].tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                    playSound(sound: "")
                }
            default:
                buttonSelected[0] = false
                buttonSelected[1] = false
                buttonSelected[2] = !buttonSelected[2]
                musicsButtons[0].tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                musicsButtons[1].tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                if buttonSelected[2]{
                    musicsButtons[2].tintColor = #colorLiteral(red: 0.4078431373, green: 0.4078431373, blue: 0.4078431373, alpha: 1)
                    playSound(sound: "sun")
                }else{
                    musicsButtons[2].tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                    playSound(sound: "")
                }
            }
        
    }
    @IBAction func back(sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func soundButtonAction(sender: UIButton){
        soundButton.isSelected = !soundButton.isSelected
        if playerGong != nil{
        playerGong.volume = soundButton.isSelected ? 1 : 0
        }
        if playerMusic != nil{
            playerMusic.volume = soundButton.isSelected ? 1 : 0
        }
    }
    func playSound(sound: String){
        if sound == ""{
            if playerMusic != nil{
                if playerMusic.isPlaying{
                    playerMusic.stop()
                }
            }
            return
        }
        guard let url = Bundle.main.url(forResource: sound, withExtension: "mp3") else { return }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            if sound == "inh-exh"{
                if playerGong != nil{
                    if playerGong.isPlaying{
                        playerGong.stop()
                    }
                }
                playerGong = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
                
                /* iOS 10 and earlier require the following line:
                 player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */
                
                guard let player = playerGong else { return }
                player.play()
                player.volume = soundButton.isSelected ? 1 : 0
                return
            }
            
            if playerMusic != nil{
                if playerMusic.isPlaying{
                    playerMusic.stop()
                }
            }
            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            playerMusic = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            
            /* iOS 10 and earlier require the following line:
             player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */
            
            guard let player = playerMusic else { return }
            player.numberOfLoops = -1
            player.play()
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if isHere{
        self.transparentVideoView.stop()
        finishTimer.invalidate()
        finishTimer = nil
        if playerGong != nil{
            gongTimer.invalidate()
            gongTimer = nil
            playerGong.stop()
        }
        if playerMusic != nil{
            playerMusic.stop()
        }
        }
    }
    
    
}
