//
//  gratefullVC.swift
//  Joy
//
//  Created by Rhonny Gonzalez on 23/5/18.
//  Copyright © 2018 Jesus Nieves. All rights reserved.
//

import UIKit

class gratefullVC: UIViewController {

    @IBOutlet weak var topLbl: UILabel!
    @IBOutlet weak var paperView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet var textFields: [UITextField]!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var bottomLbl: UILabel!
    var isFirstLayout = true
    var centerV: CGPoint!
    var frameV: CGRect!
    var gratefullArray: [String] = []
    var tap: UITapGestureRecognizer!
    var maxY: CGFloat = 0.0
    var isCompleted = false
    
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var leftConstraint: NSLayoutConstraint!
    
    var infoMinX = CGFloat(0)
    var infoWidth = CGFloat(0)

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configure()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if isFirstLayout{
            isFirstLayout = false
            configureInfo()
            centerV = CGPoint(x: self.view.center.x, y: self.view.center.y)
            frameV = self.paperView.frame
            if isCompleted{
                onlyHeart()
            }
        }
    }
    
    func configureInfo(){
        infoLabel.text = NSLocalizedString("Touch the paper to write. It will stay for a day.", comment: "")
        infoMinX = self.infoView.frame.origin.x
        infoWidth = self.infoView.frame.width
        let gradient = CAGradientLayer()
        gradient.frame = self.infoView.bounds
        gradient.colors = [#colorLiteral(red: 0.5333333333, green: 0.5058823529, blue: 0.7764705882, alpha: 1).cgColor, #colorLiteral(red: 0.1058823529, green: 0.6980392157, blue: 0.6745098039, alpha: 1).cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        self.infoView.layer.cornerRadius = 16
        self.infoView.layer.insertSublayer(gradient, at: 0)
        DispatchQueue.main.async {
            self.leftConstraint.constant = self.infoView.frame.maxX
        }
    }
    
    @IBAction func info(sender: UIButton){
        sender.isSelected = !sender.isSelected
        UIView.animate(withDuration: 1, delay: 0.0, options: [], animations: {
            self.leftConstraint.constant = sender.isSelected ? self.infoMinX : self.infoView.frame.maxX
            self.view.layoutIfNeeded()
        }, completion:{ (finished: Bool) in
            
        })
    }
    @IBAction func back(sender: UIButton){
        self.dismissScreen()
        self.dismiss(animated: true, completion: nil)
    }
    func configure(){
        gratefullArray = []
        topLbl.text = NSLocalizedString("Write five reasons why you feel grateful today:", comment: "")
        bottomLbl.text = NSLocalizedString("Thank you, thank you, thank you.", comment: "")
        if let savedGratefull = UserDefaults.standard.value(forKey: Resources.getEmailUser() + "Gratefull \(Date().preciseLocalDate)") as? [String]{
            if let isComplete = UserDefaults.standard.value(forKey: Resources.getEmailUser() + "GratefullComplete \(Date().preciseLocalDate)") as? Bool, isComplete{
                isCompleted = true
                return
            }
            gratefullArray = savedGratefull
        }else{
            if let isComplete = UserDefaults.standard.value(forKey: Resources.getEmailUser() + "GratefullComplete \(Date().preciseLocalDate)") as? Bool, isComplete{
                isCompleted = true
                return
            }
            if let lastGratefullCompleteKey = UserDefaults.standard.value(forKey: Resources.getEmailUser() + "LastGratefullComplete") as? String{
                UserDefaults.standard.removeObject(forKey: lastGratefullCompleteKey)
            }
            if let lastGratefullKey = UserDefaults.standard.value(forKey: Resources.getEmailUser() + "LastGratefull") as? String{
                UserDefaults.standard.removeObject(forKey: lastGratefullKey)
            }
            gratefullArray = ["1. ","2. ","3. ","4. ","5. "]
            let key = Resources.getEmailUser() + "Gratefull \(Date().preciseLocalDate)"
            UserDefaults.standard.setValue(gratefullArray, forKey: key)
            UserDefaults.standard.setValue(key, forKey: Resources.getEmailUser() + "LastGratefull")
        }
        for i in 0 ..< textFields.count{
            textFields[i].delegate = self
            // Define paragraph styling
            
            let paraStyle = NSMutableParagraphStyle()
            paraStyle.firstLineHeadIndent = 8.0
            
            // Apply paragraph styles to paragraph
            textFields[i].allowsEditingTextAttributes = true
            textFields[i].defaultTextAttributes = [NSAttributedStringKey.paragraphStyle.rawValue : paraStyle,
                                                    NSAttributedStringKey.font.rawValue: UIFont(name: "Ecosmith Script", size: 20.0)]
            textFields[i].attributedText = NSAttributedString(string: gratefullArray[i])

        }
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name:Notification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name:Notification.Name.UIKeyboardWillHide, object: nil)
        tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissScreen))
        self.view.addGestureRecognizer(tap)
    }
    func onlyHeart(){
        UIView.animate(withDuration: 0.5, delay: 0.0, options: [], animations: {
            self.paperView.alpha = 0
            self.topLbl.alpha = 0
            self.nextButton.alpha = 0
            self.imageView.frame.size.height = 0
            self.imageView.frame.size.width = 0
            self.imageView.center = self.paperView.center
        }, completion:{ (finished: Bool) in
            DispatchQueue.main.async {
                self.topLbl.text = NSLocalizedString("Great! Now repeat with me the most important mantra of all:", comment: "")
                self.bottomLbl.isHidden = false
                self.imageView.image = #imageLiteral(resourceName: "foldedHeart")
                self.paperView.isHidden = true
                self.nextButton.isHidden = true
            }
        })
        UIView.animate(withDuration: 0.5, delay: 0.5, options: [], animations: {
            self.topLbl.alpha = 1
            self.bottomLbl.alpha = 1
            self.imageView.frame.size.height = 224
            self.imageView.frame.size.width = 224
            self.imageView.center = self.paperView.center
        }, completion:{ (finished: Bool) in
            UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                self.imageView.frame.size.height = 179
                self.imageView.frame.size.width = 179
                self.imageView.center = self.paperView.center
            }, completion:{ (finished: Bool) in
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    self.imageView.frame.size.height = 224
                    self.imageView.frame.size.width = 224
                    self.imageView.center = self.paperView.center
                })
            })
        })
    }
    @IBAction func done(sender: Any){
        /*let renderer = UIGraphicsImageRenderer(size: self.paperView.bounds.size)
        let image = renderer.image { ctx in
            self.paperView.drawHierarchy(in: self.paperView.bounds, afterScreenUpdates: true)
        }
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFit
        imageView.image = image//UIImage.imageWithView(self.paperView)
        self.imageView.frame = self.paperView.frame
        self.paperView.isHidden = true*/
        for i in 0 ..< gratefullArray.count {
            if gratefullArray[i].count < 4{
                let alert = UIAlertController(title: NSLocalizedString("Alert", comment: ""), message: String(format: NSLocalizedString("Item %d shouldn't be empty.",comment: ""), i + 1), preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("Accept", comment: ""), style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
        }
        let key = Resources.getEmailUser() + "GratefullComplete \(Date().preciseLocalDate)"
        UserDefaults.standard.setValue(true, forKey: key)
        UserDefaults.standard.setValue(key, forKey: Resources.getEmailUser() + "LastGratefullComplete")
        UIView.animate(withDuration: 0.5, delay: 0.0, options: [], animations: {
            self.paperView.alpha = 0
            self.topLbl.alpha = 0
            self.nextButton.alpha = 0
            self.imageView.frame.size.height = 0
            self.imageView.frame.size.width = 0
            self.imageView.center = self.paperView.center
        }, completion:{ (finished: Bool) in
            DispatchQueue.main.async {
                self.topLbl.text = NSLocalizedString("Great! Now repeat with me the most important mantra of all:", comment: "")
                self.bottomLbl.isHidden = false
                self.imageView.image = #imageLiteral(resourceName: "foldedHeart")
                self.paperView.isHidden = true
                self.nextButton.isHidden = true
            }
        })
        UIView.animate(withDuration: 0.5, delay: 0.5, options: [], animations: {
            self.topLbl.alpha = 1
            self.bottomLbl.alpha = 1
            self.imageView.frame.size.height = 224
            self.imageView.frame.size.width = 224
            self.imageView.center = self.paperView.center
        }, completion:{ (finished: Bool) in
            UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                self.imageView.frame.size.height = 179
                self.imageView.frame.size.width = 179
                self.imageView.center = self.paperView.center
            }, completion:{ (finished: Bool) in
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    self.imageView.frame.size.height = 224
                    self.imageView.frame.size.width = 224
                    self.imageView.center = self.paperView.center
                })
            })
        })
    }
    func image(with view: UIView) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0.0)
        defer { UIGraphicsEndImageContext() }
        if let context = UIGraphicsGetCurrentContext() {
            view.layer.render(in: context)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            return image
        }
        return nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UIImage {
    class func imageWithView(_ view: UIView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0)
        defer { UIGraphicsEndImageContext() }
        view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
        return UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
    }
}
extension gratefullVC: UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let frame = view.convert(textField.frame, from:textField.superview!)
        print (frame)
        maxY = frame.maxY
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text == ""{
            textField.text = "\(textField.tag). "
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == ""{
            textField.text = "\(textField.tag). "
        }
        let key = Resources.getEmailUser() + "Gratefull \(Date().preciseLocalDate)"
        gratefullArray[textField.tag - 1] = textField.text!
        UserDefaults.standard.setValue(gratefullArray, forKey: key)
        UserDefaults.standard.setValue(key, forKey: Resources.getEmailUser() + "LastGratefull")
    }
    
    @objc func keyboardWillShow(notification:Notification){
        //let frame = firstView.convert(buttons.frame, from:secondView)
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            let keyboardSize2 = keyboardSize.height
            
            if maxY > self.view.frame.size.height - keyboardSize2{
                self.view.frame.origin.y = self.view.frame.size.height - keyboardSize2 - maxY - 20
            }
            
        }
        
    }
    
    @objc func keyboardWillHide(notification:Notification){
        
        /*  let contentInset:UIEdgeInsets = UIEdgeInsets.zero
         self.scrollV.contentInset = contentInset*/
        self.view.frame.origin.y = 0
    }
    
    @objc func dismissScreen(){
        self.view.endEditing(true)
    }
}

class CustomSearchTextField: UITextField {
    /*required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        
        return UIEdgeInsetsInsetRect(bounds, UIEdgeInsetsMake(0, 0, 0, 0))
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, UIEdgeInsetsMake(0, 0, 0, 0))
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, UIEdgeInsetsMake(0, 0, 0, 0))
    }*/
}

