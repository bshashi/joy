//
//  InspirationVC.swift
//  Joy
//
//  Created by Rhonny Gonzalez on 16/5/18.
//  Copyright © 2018 Jesus Nieves. All rights reserved.
//

import UIKit

class InspirationVC: UIViewController {

    @IBOutlet weak var inspirationLbl: UILabel!
    
    var inspiration: NSDictionary = [:]
    var typeLanguage = ""
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var leftConstraint: NSLayoutConstraint!
    
    var infoMinX = CGFloat(0)
    var infoWidth = CGFloat(0)
    var firstTime = true
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        typeLanguage = checkLanguage()
        if let savedInspiration = UserDefaults.standard.object(forKey: Resources.getEmailUser() + "Inspiration \(Date().preciseLocalDate)") as? NSDictionary{
            inspiration = savedInspiration
        }else{
            getInspiration()
        }
        var text = inspiration[typeLanguage] as! String
        if inspiration["Author"] as! String != ""{
            text += " - \(inspiration["Author"] as! String)"
        }
        inspirationLbl.text = text
        inspirationLbl.font = UIFont(name: "Caviar Dreams", size: 18.0)
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if firstTime{
            firstTime = false
            configureInfo()
        }
    }
    func configureInfo(){
        infoLabel.text = NSLocalizedString("Every day I offer you a bit of wisdom to apply to your life", comment: "")
        infoMinX = self.infoView.frame.origin.x
        infoWidth = self.infoView.frame.width
        let gradient = CAGradientLayer()
        gradient.frame = self.infoView.bounds
        gradient.colors = [#colorLiteral(red: 0.5333333333, green: 0.5058823529, blue: 0.7764705882, alpha: 1).cgColor, #colorLiteral(red: 0.1058823529, green: 0.6980392157, blue: 0.6745098039, alpha: 1).cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        self.infoView.layer.cornerRadius = 16
        self.infoView.layer.insertSublayer(gradient, at: 0)
        DispatchQueue.main.async {
            self.leftConstraint.constant = self.infoView.frame.maxX
        }
    }
    
    @IBAction func info(sender: UIButton){
        sender.isSelected = !sender.isSelected
        UIView.animate(withDuration: 1, delay: 0.0, options: [], animations: {
            self.leftConstraint.constant = sender.isSelected ? self.infoMinX : self.infoView.frame.maxX
            self.view.layoutIfNeeded()
        }, completion:{ (finished: Bool) in
            
        })
    }
    func getInspiration(){
        /*if let path = Bundle.main.path(forResource: "Inspiration", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? NSArray{*/
                    let jsonResult = Resources.getInspiration()
                    let dict = jsonResult.randomItem()
                    
                    inspiration = dict
                    let key = Resources.getEmailUser() + "Inspiration \(Date().preciseLocalDate)"
                    UserDefaults.standard.setValue(inspiration, forKey: key)
                    if let lastInspirationKey = UserDefaults.standard.value(forKey: Resources.getEmailUser() + "LastInspiration") as? String{
                        UserDefaults.standard.removeObject(forKey: lastInspirationKey)
                    }
                    UserDefaults.standard.setValue(key, forKey: "LastInspiration")
                /*}
            } catch {
                #if DEBUG
                print("Error getting Inspiration")
                #endif
            }
        }*/
    }
    @IBAction func back(sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func checkLanguage() -> String{
        var lenguajesUsuario:[String] = Locale.preferredLanguages
        let misLenguajes:Set<String> = ["es-","en-"]
        
        
        let index = lenguajesUsuario.index { (lenguaje) -> Bool in
            return misLenguajes.contains(String(lenguaje.prefix(3)))
        }
        
        guard let safeIndex = index else{
            
            ///ingles
            return "text-en"
        }
        
        let lenguaje = String(lenguajesUsuario[safeIndex].prefix(2))
        return "text-\(lenguaje)"
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
