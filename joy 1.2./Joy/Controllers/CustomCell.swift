//
//  CustomCell.swift
//  calendarIOS
//
//  Created by Macbook pro on 19/5/17.
//  Copyright © 2017 Rhonny Gonzalez. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CustomCell: JTAppleCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var selectedView: UIView!
    @IBOutlet weak var textFields: UITextField!
    @IBOutlet weak var normalView: UIView!
}
