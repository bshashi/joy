//
//  StaticsVC.swift
//  Joy
//
//  Created by Rhonny Gonzalez on 12/6/18.
//  Copyright © 2018 Jesus Nieves. All rights reserved.
//

import UIKit
import Charts

class StaticsVC: UIViewController {
    
    //@IBOutlet weak var chartView: PieChart!
    @IBOutlet weak var pieChart: PieChartView!
    @IBOutlet weak var profileIV: UIImageView!
    @IBOutlet weak var statisticsButton: UIButton!
    @IBOutlet weak var statisticsTV: UITableView!
    
    let tiredSick = "AFC950"
    let productiveMotivated = "7FB6D9"
    let stressed = "4240AA"
    let happyLoving = "D662A5"
    let sad = "939393"
    let peacefulRelaxed = "2EB2B1"
    let angryGrumpy = "A082DB"
    var colors: [UIColor] = []
    var arrayOfMonthsAndColor: [[String:Any]] = []
    var monthsWithColor: [[String:Any]] = []
    
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var leftConstraint: NSLayoutConstraint!

    var infoMinX = CGFloat(0)
    var infoWidth = CGFloat(0)
    var firstTime = true
    override func viewDidLoad() {
        super.viewDidLoad()
        
        statisticsButton.layer.cornerRadius = 17
        statisticsButton.layer.borderColor = UIColor.white.cgColor
        statisticsButton.layer.borderWidth = 0.5
        colors = [UIColor(hex:tiredSick)!,UIColor(hex:productiveMotivated)!,UIColor(hex:stressed)!,UIColor(hex:happyLoving)!,UIColor(hex:sad)!,UIColor(hex:peacefulRelaxed)!,UIColor(hex:angryGrumpy)!]
        // Do any additional setup after loading the view.

//
//        chartView.innerRadius = 20
//        chartView.outerRadius = 10
        
        calc()
        statisticsTV.delegate = self
        statisticsTV.dataSource = self
        
        let defaults = UserDefaults.standard
        let nameUser = defaults.string(forKey: "name")
        let stringURL = defaults.string(forKey: "image")
        if let url = URL(string: stringURL!) {
            if let url = URL(string: stringURL!) {
                self.profileIV.cacheImage(urlString: stringURL!)
            }
            //self.profileIV.kf.setImage(with: url)
        }
        
    }
    @IBAction func back(sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cmdCalendar(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cmdStats(_ sender: Any) {
        
    }
    @IBAction func showMonths(sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    func calc(){
        var colors: [Double] = [0.0,0.0,0.0,0.0,0.0,0.0,0.0]
        for i in 1 ..< arrayOfMonthsAndColor.count{
            let color = arrayOfMonthsAndColor[i]["colors"] as! [String:Int]
            colors = [colors[0] + Double(color[tiredSick]!),
                      colors[1] + Double(color[productiveMotivated]!),
                      colors[2] + Double(color[stressed]!),
                      colors[3] + Double(color[happyLoving]!),
                      colors[4] + Double(color[sad]!),
                      colors[5] + Double(color[peacefulRelaxed]!),
                      colors[6] + Double(color[angryGrumpy]!)]
            let sum = color[tiredSick]! + color[productiveMotivated]! + color[stressed]! + color[happyLoving]! + color[sad]! + color[peacefulRelaxed]! + color[angryGrumpy]!
            if sum > 0 {
                monthsWithColor.append(arrayOfMonthsAndColor[i])
            }
        }
        
        setChart(values: colors)
    }
    func setChart(values: [Double]) {
        
        var dataEntries: [ChartDataEntry] = []
        
        for i in 0..<values.count {
            let dataEntry = ChartDataEntry(x: values[i], y: values[i])//ChartDataEntry(value: values[i], xIndex: i)
            dataEntries.append(dataEntry)
        }
        
        let pieChartDataSet = PieChartDataSet(values: dataEntries, label: nil)
        let pieChartData = PieChartData(dataSet: pieChartDataSet)
        let pFormatter = NumberFormatter()
        pFormatter.numberStyle = .percent
        pFormatter.maximumFractionDigits = 1
        pFormatter.multiplier = 1
        pFormatter.percentSymbol = " %"
        pieChartData.setValueFormatter(DefaultValueFormatter(formatter: pFormatter))
        pieChart.data = pieChartData
        pieChartDataSet.valueTextColor = UIColor.clear
        pieChart.holeRadiusPercent = 0.85
        pieChart.holeColor = UIColor.clear
        pieChart.legend.enabled = false
        pieChart.chartDescription?.text = ""
        pieChart.isUserInteractionEnabled = false
        //pieChart.holeRadiusPercent = 80
        
        
        pieChartDataSet.colors = colors
        
        
        
    }
    override func viewDidLayoutSubviews() {

        profileIV.setRounded()
        if firstTime{
            firstTime = false
            configureInfo()
        }
    }
    func configureInfo(){
        infoLabel.text = NSLocalizedString("These are the annual and monthly statistics of your mood.", comment: "")
        infoMinX = self.infoView.frame.origin.x
        infoWidth = self.infoView.frame.width
        let gradient = CAGradientLayer()
        gradient.frame = self.infoView.bounds
        gradient.colors = [#colorLiteral(red: 0.5333333333, green: 0.5058823529, blue: 0.7764705882, alpha: 1).cgColor, #colorLiteral(red: 0.1058823529, green: 0.6980392157, blue: 0.6745098039, alpha: 1).cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        self.infoView.layer.cornerRadius = 16
        self.infoView.layer.insertSublayer(gradient, at: 0)
        DispatchQueue.main.async {
            self.leftConstraint.constant = self.infoView.frame.maxX
        }
    }
    
    @IBAction func info(sender: UIButton){
        sender.isSelected = !sender.isSelected
        UIView.animate(withDuration: 1, delay: 0.0, options: [], animations: {
            self.leftConstraint.constant = sender.isSelected ? self.infoMinX : self.infoView.frame.maxX
            self.view.layoutIfNeeded()
        }, completion:{ (finished: Bool) in
            
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func createChartBar(view: UIView,values: [Double]){
        var minX: CGFloat = 0
        var maxValue = 0.0
        for value in values{
            maxValue += value
        }
        let colors: [UIColor] = self.colors
        //[10, 20, 2, 1, 3]
        for i in 0 ..< values.count{
            let percent = CGFloat(((values[i] * 100.0) / maxValue) / 100.0)
            let width = view.frame.size.width * percent
            let newView = UIView(frame: CGRect(x: minX, y: 0, width: width, height: view.frame.size.height))
            minX = newView.frame.maxX
            newView.backgroundColor = colors[i]
            view.addSubview(newView)
        }
        
    }
}

extension StaticsVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return monthsWithColor.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BarChartCell", for: indexPath)
        
        let label = cell.viewWithTag(1) as! UILabel
        let barView = cell.viewWithTag(2)!
        let month = NSLocalizedString(monthsWithColor[indexPath.row]["month"] as! String, comment: "")
        let color = monthsWithColor[indexPath.row]["colors"] as! [String:Int]
        let colors = [Double(color[tiredSick]!), Double(color[productiveMotivated]!), Double(color[stressed]!), Double(color[happyLoving]!), Double(color[sad]!),Double(color[peacefulRelaxed]!),Double(color[angryGrumpy]!)]
        
        label.text = month
        barView.layer.cornerRadius = 10
        createChartBar(view: barView, values: colors)
        
        
        return cell
    }
}

