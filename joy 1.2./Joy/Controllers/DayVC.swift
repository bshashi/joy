//
//  DayVC.swift
//  calendarIOS
//
//  Created by Rhonny Gonzalez on 18/5/18.
//  Copyright © 2018 Rhonny Gonzalez. All rights reserved.
//

import UIKit

class DayVC: UIViewController{
    

    @IBOutlet weak var centerView: UIView!
    @IBOutlet weak var dayOfWeek: UILabel!
    @IBOutlet weak var day: UILabel!
    @IBOutlet weak var month: UILabel!
    @IBOutlet var buttons: [UIButton]!
    @IBOutlet weak var background: UIImageView!
    var originalLocation: CGPoint!
    var changeColor = false
    var dateSelected = ""
    var color = UIColor()
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    func configure(){
        let dateArray = dateSelected.components(separatedBy: "-")
        dayOfWeek.text = dateArray[0].capitalized
        day.text = dateArray[1]
        month.text = dateArray[2].uppercased()
        centerView.backgroundColor = color
        if color == UIColor.clear{
            centerView.layer.borderColor = UIColor.white.cgColor
        }
        for i in 0 ..< buttons.count{
            let pangesture = UIPanGestureRecognizer(target: self, action: #selector(dragView(sender:)))
            buttons[i].tag = i
            buttons[i].addGestureRecognizer(pangesture)
        }
        
    }
    @IBAction func back(sender: UIButton){
        let notificationName = Notification.Name("MoodColor")
        
        NotificationCenter.default.post(name: notificationName, object: centerView.backgroundColor)
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        centerView.layer.cornerRadius = centerView.frame.size.width / 2
        
    }
    
    @objc func dragView(sender: UIPanGestureRecognizer){
        let item = sender.view!.tag
        if sender.state == .began{
            originalLocation = buttons[item].center
        }
        self.view.bringSubview(toFront: buttons[item])
        let trans = sender.translation(in: buttons[item])
        buttons[item].center = CGPoint(x: buttons[item].center.x + trans.x, y: buttons[item].center.y + trans.y)
        sender.setTranslation(CGPoint.zero, in: self.view)
        if sender.state == .ended{
            let x1 = centerView.frame.origin.x
            let y1 = centerView.frame.origin.y
            let l = centerView.frame.size.width
            let x = buttons[item].center.x
            let y = buttons[item].center.y
            if x1<=x && x<=x1+l && y1<=y && y<=y1+l{
                changeColor = true
            }
            UIView.animate(withDuration: 0.5, delay: 0.1, options: [], animations: { () -> Void in
                if self.changeColor{
                self.centerView.layer.borderColor = UIColor.clear.cgColor
                self.centerView.backgroundColor = self.buttons[item].backgroundColor!
                    self.changeColor = false
                }
                self.buttons[item].center = self .originalLocation
            })
        }
    }
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
