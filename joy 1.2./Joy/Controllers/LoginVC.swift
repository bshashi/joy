//
//  LoginVC.swift
//  Joy
//
//  Created by Jesus Nieves on 6/8/18.
//  Copyright © 2018 Jesus Nieves. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import NVActivityIndicatorView

class LoginVC: BaseViewController {
    
    var ref:DatabaseReference!
    @IBOutlet weak var txtEmail: JoyLoginTextField!
    @IBOutlet weak var txtPass: JoyLoginTextField!
    @IBOutlet var languageButtons: [UIButton]!
    var languageSelected = 0
    var emailString = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        txtEmail.text = emailString
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        if L102Language.currentAppleLanguage().contains(L102Language.languages()[0]){
            languageButtons[0].backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.3032427226)
            languageSelected = 0
        }else{
            languageButtons[1].backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.3032427226)
            languageSelected = 1
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SegueWelcomeBack"{
            let WBVC = segue.destination as! WelcomeBackVC
            //FIXME: CHANGE THIS TO USER NAME
            WBVC.name = txtEmail.text!
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func forgotPressed(_ sender: Any) {
        let alert = UIAlertController(title: "Recover password".localized, message: "Enter your email".localized, preferredStyle: .alert)
        
        alert.addTextField { (textField) in

        }
        alert.addAction(UIAlertAction(title: "Accept".localized, style: .default, handler: { [weak alert] (_) in
            let email = alert?.textFields![0].text
            Auth.auth().sendPasswordReset(withEmail: email!) { (error) in
                if (error != nil) {
                    let alertError = UIAlertController(title: "Error".localized, message: "You are not registered".localized, preferredStyle: .alert)
                    alertError.addAction(UIAlertAction(title: "Accept".localized, style: .default, handler: nil))
                    self.present(alertError, animated: true, completion: nil)
                }
                let alertSuccess = UIAlertController(title: "Success".localized, message: "An email has been sent with the instructions to recover the password".localized, preferredStyle: .alert)
                alertSuccess.addAction(UIAlertAction(title: "Accept".localized, style: .default, handler: nil))
                self.present(alertSuccess, animated: true, completion: nil)
            }
        }))
        
        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
//        Auth.auth().sendPasswordReset(withEmail: email) { (error) in
//            // ...
//        }
    }
    
    @IBAction func signInPressed(_ sender: Any) {
    let valid = validSingIn()
    if valid.0{
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(self.activityData)
        
        Auth.auth().signInAndRetrieveData(withEmail: txtEmail.text!, password: txtPass.text!) { (user, error) in

            if ((error) != nil) {
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                if error!.localizedDescription.contains("password is invalid"){
                    self.showAlert(title: "Error", message: "Invalid Password.")
                }else if error!.localizedDescription.contains("There is no user record corresponding to this identifier."){
                    self.showAlert(title: "Error", message: "User doesn't exist.")
                }else{
                    self.showAlert(title: "Error", message: error!.localizedDescription)
                }
            } else {
                
                self.ref = Database.database().reference()
                NVActivityIndicatorPresenter.sharedInstance.startAnimating(self.activityData)
                self.ref.child("users").child((Auth.auth().currentUser?.uid)!).observeSingleEvent(of: .value, with: {(snap) in
                    if snap.exists(){
                        let value = snap.value as? NSDictionary
                        let name:String = (value?["name"] as? String)!
                        let defaults = UserDefaults.standard
                        defaults.set(name, forKey: "name")
                        let keyExists = value?["picture"] != nil
                        var stringURL = ""
                        if (keyExists) {
                            stringURL = value?["picture"] as! String
                            
                        }
                        defaults.set(stringURL, forKey: "image")
                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                        self.performSegue(withIdentifier: "SegueWelcomeBack", sender: self)
                    } else {
                       NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                        self.showAlert(title: "Error", message: "User doesn't exist.")
                    }
                })

                //self.navigationController?.dismiss(animated: true, completion: nil)
            }
        }
        }else{
            showAlert(title: "Error", message: valid.1)
        }
    }
    func validSingIn() -> (Bool,String){
        
        guard txtEmail.text! != "" else{
            return (false,"Email can't be empty.")
        }
        guard txtEmail.text!.isValidEmail() else{
            return (false,"The email address is badly formatted.")
        }
        guard txtPass.text! != "" else{
            return (false,"Password can't be empty.")
        }
        
        return (true,"")
    }
    func showAlert(title:String,message:String){
        let alertController = UIAlertController(
            title: NSLocalizedString(title, comment: ""),
            message: NSLocalizedString(message, comment: ""),
            preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: NSLocalizedString("Ok",comment: ""), style: .cancel, handler: nil)
        alertController.addAction(okAction)
  
        self.present(alertController, animated: true, completion: nil)
    }

    @objc func dismissKeyboard(){
        self.view.endEditing(true)
    }
    
    @IBAction func switchLanguage(sender: UIButton){
        if languageSelected != sender.tag{
            let alertController = UIAlertController(
                title: NSLocalizedString("Alert", comment: ""),
                message: NSLocalizedString("If you change the language, you must restart the app to see results.", comment: ""),
                preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: NSLocalizedString("Ok",comment: ""), style: .default) { (action) in
            
                for i in 0 ..< self.languageButtons.count{
                    
                    if i == sender.tag{
                        
                        self.languageButtons[i].layer.borderWidth = 0
                        self.languageButtons[i].backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.3032427226)
                        L102Language.setAppleLAnguageTo(lang: i)
                        self.languageButtons[i].layer.cornerRadius = 12.5
                        self.languageSelected = i
                    }else{
                        self.languageButtons[i].layer.borderWidth = 1
                        self.languageButtons[i].backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.01)
                        self.languageButtons[i].layer.borderColor = UIColor.white.cgColor
                        self.languageButtons[i].layer.cornerRadius = 12.5
                        self.languageSelected = i
                    }
                }
            
            }
            
            alertController.addAction(okAction)
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        
        }
        
    }
}
