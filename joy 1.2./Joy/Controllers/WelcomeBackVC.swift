//
//  WelcomeBackVC.swift
//  Joy
//
//  Created by Rhonny Gonzalez on 28/8/18.
//  Copyright © 2018 Jesus Nieves. All rights reserved.
//

import UIKit

class WelcomeBackVC: UIViewController {

    @IBOutlet weak var welcomeBackLbl: UILabel!
    
    var name = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        var i = 1
        let todayDate = Date().preciseLocalDate
        if let days = UserDefaults.standard.value(forKey: Resources.getEmailUser() + "DaysOpen") as? Int{
            if let savedDay = UserDefaults.standard.value(forKey: Resources.getEmailUser() + "LastOpenDate") as? String{
                if todayDate != savedDay{
                    i = days + 1
                }else{
                    i = days
                }
            }
            
        }
        
        let defaults = UserDefaults.standard
        let nameUser = defaults.string(forKey: "name")
        
        UserDefaults.standard.set(todayDate, forKey: Resources.getEmailUser() + "LastOpenDate")
        UserDefaults.standard.set(i, forKey: Resources.getEmailUser() + "DaysOpen")
        welcomeBackLbl.text = String(format: "Hey! I'm glad to see you %@. How are you? Ready to be the best version of yourself? And remember: trust the process, you're doing great.".localized, nameUser!)
        
    }
    
    @IBAction func next(sender: UIButton){
        self.performSegue(withIdentifier: "welcome22", sender: self)
//        self.navigationController?.dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
