//
//  NotebookPageVC.swift
//  Joy
//
//  Created by Rhonny Gonzalez on 29/5/18.
//  Copyright © 2018 Jesus Nieves. All rights reserved.
//

import UIKit
import RealmSwift
import FirebaseAuth

class NotebookPageVC: UIPageViewController {

    var notebookKey = "Notebook2018"
    var notebookDateKey = ""
    private var currentNotebook:Notebook!
    var viewControllerSaved = UIViewController()
    private var currentIndex = 0
    private let realm = try! Realm()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataSource = self
        let nextPage = Notification.Name("NextPage")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.nextPage(notification:)), name: nextPage, object: nil)
        
        let loadViewControllers = Notification.Name("LoadViewControllers")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.loadViewControllers(notification:)), name: loadViewControllers, object: nil)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func loadViewControllers(notification: Notification){
        notebookKey = UserDefaults.standard.value(forKey: Resources.getEmailUser() + "NotebookKey") as! String
        orderedViewControllers = []
        orderedViewControllers = viewControllers()
        
        self.currentIndex = 0
        UserDefaults.standard.set(self.currentIndex, forKey:"CurrentIndex")
        
        if let direction = notification.object as? String, direction == "reverse"{
            setViewControllers([orderedViewControllers[0]], direction: .reverse, animated: true, completion: nil)
        }
        setViewControllers([orderedViewControllers[0]], direction: .forward, animated: true, completion: nil)
 
    }
    func viewControllers() -> [UIViewController]{
        
        var viewControllers: [UIViewController] = []
        
        if let key = UserDefaults.standard.value(forKey: Resources.getEmailUser() + "notebookDate") as? String{
            notebookDateKey = key
        }else{
            let formatter = DateFormatter()
            formatter.timeZone = TimeZone(secondsFromGMT: 0)
            formatter.dateFormat = "dd/MM/yyyy"
            let todayDate = formatter.date(from: "\(Date().preciseLocalDate)")
            notebookDateKey = todayDate!.preciseGMTDate
        }
        UserDefaults.standard.setValue(notebookDateKey, forKey: Resources.getEmailUser() + "notebookDate")
        

        let predicate = NSPredicate(format: "key == %@ && user == %@", notebookDateKey, (Auth.auth().currentUser?.uid)!)
        self.currentNotebook = self.realm.objects(Notebook.self).filter(predicate).first
        
        if self.currentNotebook?.pages.count == 0 {
            let firstPage = NotebookPage()
            firstPage.content = ""
            try! realm.write {
                self.currentNotebook?.pages.append(firstPage)
            }
            viewControllers.append(self.newPage())
        } else {
            for page in self.currentNotebook.pages {
                viewControllers.append(self.newPage())
            }
        }
        return viewControllers
        
    }
    var orderedViewControllers: [UIViewController] = []
    private func newPage() -> UIViewController {
        return UIStoryboard(name: "Cuaderno", bundle: nil).instantiateViewController(withIdentifier: "notebookView")
    }
    
    @objc func nextPage(notification: Notification){
        if self.currentNotebook.pages.count < (currentIndex + 2) {
            let otherPage = NotebookPage()
            otherPage.content = ""
            try! realm.write {
                self.currentNotebook?.pages.append(otherPage)
            }
            self.orderedViewControllers.append(self.newPage())
        }
    
        self.currentIndex += 1
        UserDefaults.standard.set(self.currentIndex, forKey:"CurrentIndex")
        setViewControllers([orderedViewControllers[self.currentIndex]], direction: .forward, animated: true, completion: nil)

    }
}

// MARK: UIPageViewControllerDataSource

extension NotebookPageVC: UIPageViewControllerDataSource,UIPageViewControllerDelegate {
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        if (self.currentIndex - 1) >= 0 {
            self.currentIndex -= 1
            UserDefaults.standard.set(self.currentIndex, forKey:"CurrentIndex")
            return orderedViewControllers[self.currentIndex]
        } else {
            return nil
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if self.currentNotebook.pages.count < (currentIndex + 2) {
            let otherPage = NotebookPage()
            otherPage.content = ""
            try! realm.write {
                self.currentNotebook?.pages.append(otherPage)
            }
            self.orderedViewControllers.append(self.newPage())
        }
        
        self.currentIndex += 1
        UserDefaults.standard.set(self.currentIndex, forKey:"CurrentIndex")
        return orderedViewControllers[self.currentIndex]
    }
    
}
