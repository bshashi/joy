//
//  WelcomeVC.swift
//  Joy
//
//  Created by Rhonny Gonzalez on 28/8/18.
//  Copyright © 2018 Jesus Nieves. All rights reserved.
//

import UIKit

class WelcomeVC: UIViewController {

    @IBOutlet weak var welcomeLbl: UILabel!
    
    var name = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        var i = 1
        let todayDate = Date().preciseLocalDate
        let defaults = UserDefaults.standard
        let nameUser = defaults.string(forKey: "name")

        if let days = UserDefaults.standard.value(forKey: Resources.getEmailUser() + "DaysOpen") as? Int{
            if let savedDay = UserDefaults.standard.value(forKey: Resources.getEmailUser() + "LastOpenDate") as? String{
                if todayDate != savedDay{
                    i = days + 1
                }else{
                    i = days
                }
            }
            
        }
        
        UserDefaults.standard.set(todayDate, forKey: Resources.getEmailUser() + "LastOpenDate")
        UserDefaults.standard.set(i, forKey: Resources.getEmailUser() + "DaysOpen")
        welcomeLbl.text = String(format: "Hi %@! I am Joy. From now on we start a new path together. These are the tools that I offer you so you can start taking control of your life.".localized, nameUser!)
    }

    @IBAction func next(sender: UIButton){
        self.performSegue(withIdentifier: "welcome2", sender: self)
//        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
