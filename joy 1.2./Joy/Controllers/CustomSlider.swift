//
//  CustomSlider.swift
//  Joy
//
//  Created by Rhonny Gonzalez on 7/6/18.
//  Copyright © 2018 Jesus Nieves. All rights reserved.
//

import UIKit

class CustomSlider: UISlider {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBInspectable var trackHeight: CGFloat = 2

    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        
        return CGRect(origin: bounds.origin, size: CGSize(width: bounds.width, height: trackHeight))
    
    }

}
