//
//  SubscripcionVC.swift
//  Joy
//
//  Created by Jesus Nieves on 10/10/18.
//  Copyright © 2018 Jesus Nieves. All rights reserved.
//

import UIKit
import StoreKit
import RealmSwift
import NVActivityIndicatorView

class SubscripcionVC: BaseViewController  {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnBuy: DesignableButton!
    var showBack = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        IAPHandler.shared.fetchAvailableProducts()
        IAPHandler.shared.purchaseStatusBlock = {[weak self] (type) in
            guard let strongSelf = self else { return }
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            switch type {
            case .purchased, .restored:
                let alertView = UIAlertController(title: "", message: type.message(), preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                    let realm = try! Realm()
                    let purchase = Purchased()
                    purchase.purchased = true
                    try! realm.write {
                        realm.add(purchase)
                    }
                    strongSelf.dismiss()
                })
                alertView.addAction(action)
                if var topController = UIApplication.shared.keyWindow?.rootViewController {
                    while let presentedViewController = topController.presentedViewController {
                        topController = presentedViewController
                    }
                    topController.present(alertView, animated: true, completion: nil)
                }
            default:
                let alertView = UIAlertController(title: "", message: type.message(), preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                })
                alertView.addAction(action)
                if var topController = UIApplication.shared.keyWindow?.rootViewController {
                    while let presentedViewController = topController.presentedViewController {
                        topController = presentedViewController
                    }
                    topController.present(alertView, animated: true, completion: nil)
                }
            }
        }
        
        IAPHandler.shared.purchaseProductsBlock = {[weak self] (products) in
            guard let strongSelf = self else{ return }
            if products.count > 0 {
                let product = products[0]
                let numberFormatter = NumberFormatter()
                numberFormatter.formatterBehavior = .behavior10_4
                numberFormatter.numberStyle = .currency
                numberFormatter.locale = product.priceLocale
                let price1Str = numberFormatter.string(from: product.price)
                let text = String(format: "%@! per year.".localized, price1Str!)
                strongSelf.btnBuy.setTitle("\(text)", for: UIControlState.normal)
                print(text)
            } else {
                let alertView = UIAlertController(title: "", message: "Not product found", preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                })
                alertView.addAction(action)
                strongSelf.present(alertView, animated: true, completion: nil)
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if showBack == true {
            self.btnBack.isHidden = false
        } else {
            self.btnBack.isHidden = true
        }
    }
    func dismiss() {
        print("dismiss")
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            topController.dismiss(animated: true, completion: nil)
            // topController should now be your topmost view controller
        }
    }
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        for product in response.products {
            print("Price: \(product)")
        }
    }

    
    @IBAction func termsPressed(_ sender: Any) {
        guard let url = URL(string: "https://www.findjoyapp.com/terms") else {
            return
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    
    @IBAction func privacyPressed(_ sender: Any) {
        guard let url = URL(string: "https://www.findjoyapp.com/privacy") else {
            return
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func buyPressed(_ sender: Any) {
        print("realizar compra")
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(self.activityData)
        IAPHandler.shared.purchaseMyProduct(index: 0)
    }
    
    @IBAction func restorePurchasesPressed(_ sender: Any) {
         NVActivityIndicatorPresenter.sharedInstance.startAnimating(self.activityData)
         IAPHandler.shared.restorePurchase()
    }
    
    @IBAction func btnBackPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
