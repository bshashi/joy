//
//  Notebook.swift
//  Joy
//
//  Created by JESUS NIEVES on 14/11/18.
//  Copyright © 2018 Jesus Nieves. All rights reserved.
//
import Foundation
import RealmSwift

class Notebook: Object {
    @objc dynamic var user = ""
    @objc dynamic var key = ""
    let pages = List<NotebookPage>()
}
