//
//  Purchased.swift
//  Joy
//
//  Created by JESUS NIEVES on 01/12/2018.
//  Copyright © 2018 Jesus Nieves. All rights reserved.
//

import Foundation
import RealmSwift

class Purchased: Object {
    @objc dynamic var purchased = false
}
