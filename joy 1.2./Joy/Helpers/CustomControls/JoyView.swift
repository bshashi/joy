//
//  JoyView.swift
//  Joy
//
//  Created by Jesus Nieves on 10/3/18.
//  Copyright © 2018 Jesus Nieves. All rights reserved.
//

import UIKit

@IBDesignable class JoyView: UIView {

    @IBInspectable var background: UIImage = UIImage() {
        didSet {
            setNeedsLayout()
        }
    }

    override func layoutSubviews() {
        UIGraphicsBeginImageContext(frame.size)
        background.draw(in: bounds)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        backgroundColor = UIColor(patternImage: image)

    }
    
}
